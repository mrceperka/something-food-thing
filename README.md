# Something food :meat_on_bone:
![Something food logo](./logo.jpg "Something food logo")
## Setup
* `docker-compose up -d` - start composition
* update your hosts file (eg. `/etc/hosts`):
```text
172.99.0.10	something.docker
172.99.0.11	api.something.docker
```
* `docker-compose exec rust diesel database reset` - reset db
* Visit [something.docker](http://something.docker:3000)
* Visit [api.something.docker/graphiql](http://api.something.docker:8000/graphiql)

## Commands
### Rust
* `docker-compose exec rust bash` - jump into container
* `docker-compose exec rust diesel database reset` - reset db
* `docker-compose exec rust cargo run` - start project
### Node
* `docker-compose exec node bash` - jump into container
* `docker-compose exec node yarn dev` - start project