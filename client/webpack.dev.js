const merge = require('webpack-merge')
const common = require('./webpack.common.js')

const ip = process.env.SOMETHING_SERVICE_RUST_IPV4 || 'rust'
const port = process.env.SOMETHING_SERVICE_RUST_PUBLIC_PORT || 8000
module.exports = merge(common, {
  mode: 'development',
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    host: '0.0.0.0',
    port: process.env.SOMETHING_SERVICE_NODE_PUBLIC_PORT || 3000,
    historyApiFallback: true,
    public:
      process.env.SOMETHING_SERVICE_NODE_PUBLIC_HOST || 'something.docker',
    // disableHostCheck: true,
    proxy: {
      '/graphql': {
        target: `http://${ip}:${port}/`,
        secure: false,
      },
    },
  },
})
