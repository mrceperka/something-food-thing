import React from 'react'
import i18n from '@intl'
const Footer = () => {
  return (
    <footer>
      <div>
        {i18n.t('Icons made by')}{' '}
        <a
          href="https://www.flaticon.com/authors/smashicons"
          title="Smashicons"
        >
          Smashicons
        </a>
        ,{' '}
        <a href="https://www.flaticon.com/authors/turkkub" title="turkkub">
          turkkub
        </a>
        ,{' '}
        <a href="https://www.freepik.com/" title="Freepik">
          Freepik
        </a>{' '}
        from{' '}
        <a href="https://www.flaticon.com/" title="Flaticon">
          www.flaticon.com
        </a>{' '}
        is licensed by{' '}
        <a
          href="http://creativecommons.org/licenses/by/3.0/"
          title="Creative Commons BY 3.0"
          target="_blank"
        >
          CC 3.0 BY
        </a>
      </div>
    </footer>
  )
}
export default Footer
