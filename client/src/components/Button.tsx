import styled from '@emotion/styled'
import React from 'react'

const Button = styled.button`
  color: black;
  cursor: pointer;
  background: none;
  border: 1px solid black;
`
export default Button
