import intl from 'i18next'
// @ts-ignore
import LanguageDetector from 'i18next-browser-languagedetector'

intl.use(LanguageDetector).init({
  // we init with resources
  resources: {
    en: {
      translations: {
        'Icons made by': 'Icons made by',
        'Select ingredients': 'Select ingredients',
      },
    },
    cs: {
      translations: {
        'Icons made by': 'Ikonky vyrobili',
        'Select ingredients': 'Vyberte ingredience',
      },
    },
  },
  fallbackLng: 'en',
  debug: true,

  // have a common namespace used around the full app
  ns: ['translations'],
  defaultNS: 'translations',

  keySeparator: false, // we use content as keys

  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: ',',
  },

  react: {
    wait: true,
  },
})

export default intl
