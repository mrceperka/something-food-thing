/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FilterIngredientsQuery
// ====================================================

export interface FilterIngredientsQuery_filterIngredients {
  __typename: "Ingredient";
  id: number;
  uniqueName: string;
  name: string | null;
}

export interface FilterIngredientsQuery {
  filterIngredients: FilterIngredientsQuery_filterIngredients[];
}

export interface FilterIngredientsQueryVariables {
  filter: IngredientFilterGqlInput;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FilteRecipesQuery
// ====================================================

export interface FilteRecipesQuery_filterRecipes {
  __typename: "Recipe";
  id: number;
  name: string | null;
}

export interface FilteRecipesQuery {
  filterRecipes: FilteRecipesQuery_filterRecipes[];
}

export interface FilteRecipesQueryVariables {
  filter: RecipeFilterGqlInput;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum RecipeDifficulty {
  easy = "easy",
  hard = "hard",
  medium = "medium",
}

/**
 * Ingredient filter
 */
export interface IngredientFilterGqlInput {
  name?: string | null;
  languageId?: number | null;
}

/**
 * A complex search for recipes
 */
export interface RecipeFilterGqlInput {
  name?: string | null;
  containIngredients?: number[] | null;
  notContainIngredients?: number[] | null;
  difficulty?: RecipeDifficulty | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
