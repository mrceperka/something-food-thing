import React from 'react'
import { render } from 'react-dom'
import Footer from '@components/Footer/Footer'
import { ApolloProvider } from 'react-apollo'
import { client } from './apollo/client'
import HomePage from '@pages/HomePage'
import { I18nextProvider } from 'react-i18next'
import i18n from '@intl'

const App = () => (
  <I18nextProvider i18n={i18n}>
    <ApolloProvider client={client}>
      <h1>Something food</h1>
      <HomePage />
      <Footer />
    </ApolloProvider>
  </I18nextProvider>
)

render(<App />, document.getElementById('root'))

// @ts-ignore
if (module.hot) module.hot.accept()
