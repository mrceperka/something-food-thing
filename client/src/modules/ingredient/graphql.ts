import gql from 'graphql-tag'

export const FILTER_QUERY = gql`
  query FilterIngredientsQuery($filter: IngredientFilterGqlInput!) {
    filterIngredients(filter: $filter) {
      id
      uniqueName
      name
    }
  }
`
