import React from 'react'

type Props = { uniqueName: string }
const Icon: React.FunctionComponent<Props> = ({ uniqueName }) => {
  try {
    return (
      <div
        style={{ width: 24, height: 24 }}
        dangerouslySetInnerHTML={{
          __html: require(`../icons/${uniqueName}.svg`),
        }}
      />
    )
  } catch (e) {
    return <> "No ico"</>
  }
}
export default Icon
