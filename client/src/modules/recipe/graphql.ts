import gql from 'graphql-tag'

export const FILTER_QUERY = gql`
  query FilteRecipesQuery($filter: RecipeFilterGqlInput!) {
    filterRecipes(filter: $filter) {
      id
      name
    }
  }
`
