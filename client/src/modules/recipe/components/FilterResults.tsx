import { FilteRecipesQuery, FilteRecipesQueryVariables } from '@schema-types'
import React from 'react'
import { FILTER_QUERY } from '../graphql'
import { Query } from 'react-apollo'

type Props = { filter: FilteRecipesQueryVariables['filter'] }
const FilterResults: React.StatelessComponent<Props> = ({ filter }: Props) => {
  return (
    <Query<FilteRecipesQuery, FilteRecipesQueryVariables>
      query={FILTER_QUERY}
      variables={{ filter }}
    >
      {({ loading, error, data }) => {
        if (loading) return <div>Fetching</div>
        if (error) return <div>Error</div>
        return data.filterRecipes.map(r => {
          return (
            <div key={r.id}>
              id: {r.id}
              name: {r.name}
            </div>
          )
        })
      }}
    </Query>
  )
}
export default FilterResults
