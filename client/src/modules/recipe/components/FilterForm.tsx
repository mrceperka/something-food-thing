import Icon from '@modules/ingredient/components/Icon'
import {
  FilteRecipesQueryVariables,
  FilterIngredientsQuery_filterIngredients,
} from '@schema-types'
import Downshift, { DownshiftProps } from 'downshift'
import { Formik } from 'formik'
import React from 'react'
import intl from '@intl'
import Button from '@components/Button'

const stateReducer: DownshiftProps<
  FilterIngredientsQuery_filterIngredients
>['stateReducer'] = (state, changes) => {
  // this prevents the menu from being closed when the user
  // selects an item with a keyboard or mouse
  switch (changes.type) {
    case Downshift.stateChangeTypes.blurInput:
    case Downshift.stateChangeTypes.keyDownEnter:
    case Downshift.stateChangeTypes.clickItem:
      return {
        ...changes,

        inputValue: '',
      }
    default:
      return changes
  }
}

type IngredientId = FilterIngredientsQuery_filterIngredients['id']

type Props = {
  ingredients: FilterIngredientsQuery_filterIngredients[]
  onSubmit: (v: FilteRecipesQueryVariables['filter']) => void
}
type Values = {
  include: Map<IngredientId, IngredientId>
}

const FilterForm = ({ ingredients, onSubmit }: Props) => {
  return (
    <div>
      <Formik<Values>
        initialValues={{ include: new Map() }}
        onSubmit={async (values, { setSubmitting }) => {
          const include = Array.from(values.include.keys())
          console.log(include)

          onSubmit({
            containIngredients: include,
          })
          setSubmitting(false)
        }}
      >
        {({
          values,

          handleSubmit,
          isSubmitting,
          setFieldValue,
          resetForm,
        }) => (
          <form onSubmit={handleSubmit}>
            <Downshift
              stateReducer={stateReducer}
              onSelect={(v: FilterIngredientsQuery_filterIngredients) => {
                const include = new Map(values.include)
                if (include.has(v.id)) {
                  include.delete(v.id)
                } else {
                  include.set(v.id, v.id)
                }
                setFieldValue('include', include)
              }}
              itemToString={item => (item ? item.name : '')}
            >
              {({
                getInputProps,
                getItemProps,
                getLabelProps,
                getMenuProps,
                isOpen,
                inputValue,
                highlightedIndex,
                selectedItem,
              }) => {
                return (
                  <div>
                    <label {...getLabelProps()}>
                      {intl.t('Select ingredients')}
                    </label>
                    <input {...getInputProps()} />
                    <ul {...getMenuProps()}>
                      {isOpen &&
                        ingredients
                          .filter(
                            item =>
                              !inputValue || item.name.includes(inputValue)
                          )
                          .map((item, index) => (
                            <li
                              {...getItemProps({
                                key: item.id,
                                index,
                                item,
                                style: {
                                  backgroundColor: values.include.has(item.id)
                                    ? 'lightgray'
                                    : undefined,
                                  border:
                                    highlightedIndex === index
                                      ? '1px solid blue'
                                      : undefined,
                                },
                              })}
                            >
                              <Icon uniqueName={item.uniqueName} />
                              {values.include.has(item.id) ? 'xxxxx' : null}
                              {item.name}
                            </li>
                          ))}
                    </ul>
                  </div>
                )
              }}
            </Downshift>

            <Button type="submit" disabled={isSubmitting}>
              Submit
            </Button>
            <Button type="button" onClick={() => resetForm()}>
              Reset
            </Button>
          </form>
        )}
      </Formik>
    </div>
  )
}

export default FilterForm
