import { FILTER_QUERY } from '@modules/ingredient/graphql'
import {
  FilteRecipesQueryVariables,
  FilterIngredientsQuery,
  FilterIngredientsQueryVariables,
} from '@schema-types'
import React from 'react'
import { Query } from 'react-apollo'

import FilterForm from './FilterForm'
import FilterResults from './FilterResults'

type State = {
  filter: FilteRecipesQueryVariables['filter']
}
type Props = any
class Filter extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { filter: {} }
  }
  render() {
    return (
      <Query<FilterIngredientsQuery, FilterIngredientsQueryVariables>
        query={FILTER_QUERY}
        variables={{ filter: {} }}
      >
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>
          return (
            <>
              <FilterForm
                ingredients={data.filterIngredients}
                onSubmit={filter => this.setState({ filter })}
              />
              <FilterResults filter={this.state.filter} />
            </>
          )
        }}
      </Query>
    )
  }
}
export default Filter
