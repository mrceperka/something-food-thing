#[derive(Debug)]
pub struct Config {
    pub paseto_secret: String,
    pub database_url: String
}

use std::env;

lazy_static! {
    pub static ref CONFIG: Box<Config> = Box::new(
    Config {
        paseto_secret: env::var("PASETO_SECRET").expect("PASETO_SECRET is not defined"),
        database_url: env::var("DATABASE_URL").expect("DATABASE_URL is not defined")
    });
}