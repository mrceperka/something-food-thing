use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::unit;
use crate::modules::unit::gql::entity::NewUnitGqlInput;

pub type UnitName = String;
pub type UnitId = i32;

#[table_name = "unit"]
#[derive(Serialize, Queryable, Debug, Clone)]
pub struct Unit {
    pub id: UnitId,
    pub name: UnitName,
}


#[table_name = "unit"]
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
pub struct NewUnit {
    pub name: UnitName,
}

impl From<NewUnitGqlInput> for NewUnit {
    fn from(input: NewUnitGqlInput) -> Self {
        NewUnit {
            name: input.unique_name,
        }
    }
}

impl Unit {
    pub fn get_by_id(id: &UnitId, conn: &DbConnection) -> Result<Unit, Error> {
        unit::table
            .find(id)
            .first(conn)
    }

    pub fn create(item: NewUnit, conn: &DbConnection) -> Result<Unit, Error> {
        diesel::insert_into(unit::table)
            .values(&item)
            .get_result(conn)
    }
}