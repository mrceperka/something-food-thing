use diesel::prelude::*;
use diesel::result::Error;
use juniper::{Executor, FieldError, FieldResult};

use crate::graphql::context::GraphQLContext;
use crate::modules::unit::gql::entity::NewUnitGqlInput;
use crate::modules::unit::gql::entity::UnitGql;
use crate::modules::unit::model::NewUnit;
use crate::modules::unit::model::Unit;
use crate::modules::unit_translation::model::{NewUnitTranslation, UnitTranslation};

pub fn create_resolver(input: NewUnitGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<UnitGql> {
    let ctx = executor.context();
    let conn = &ctx.db_pool.get()?;

    match conn.transaction::<Unit, Error, _>(|| {
        let unit = Unit::create(NewUnit::from(input.clone()), conn)?;

        for tr in input.translations {
            UnitTranslation::create(NewUnitTranslation {
                unit_id: unit.id,
                language_id: tr.language_id,
                name: tr.name,
            }, conn)?;
        }
        Ok(unit)
    }) {
        Ok(v) => Ok(UnitGql::from(v)),
        _ => Err(FieldError::from("Db failure"))
    }
}
