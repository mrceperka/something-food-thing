use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::unit::gql::entity::UnitGql;
use crate::modules::unit::model::Unit;
use crate::modules::unit::model::UnitId;
use crate::modules::unit_translation::model::UnitTranslationName;
use crate::modules::unit_translation::model::UnitTranslation;

pub fn get_by_id_resolver(id: &UnitId, executor: &Executor<GraphQLContext>) -> FieldResult<UnitGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let unit = Unit::get_by_id(id, &conn)?;

    Ok(UnitGql::from(unit))
}

pub fn name_resolver(id: &UnitId, executor: &Executor<GraphQLContext>) -> FieldResult<Option<UnitTranslationName>> {
    let ctx = executor.context();
    let lang = &ctx.language;
    let conn = ctx.db_pool.get()?;
    match UnitTranslation::get_by_unit_id_and_language_id(id, &lang.id, &conn) {
        Ok(tr) =>  Ok(Some(tr.name)),
        _ =>  Ok(None)
    }
}