use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::unit::gql::query_resolver::name_resolver;
use crate::modules::unit::model::Unit;
use crate::modules::unit::model::UnitId;
use crate::modules::unit::model::UnitName;
use crate::modules::unit_translation::model::UnitTranslationName;
use crate::modules::unit_translation::gql::entity::NewUnitTranslationNestedGqlInput;

#[graphql(description = "An unit, used in recipe preparations steps")]
pub struct UnitGql {
    pub id: UnitId,
    pub unique_name: UnitName,
    pub name: Option<UnitTranslationName>,
}

#[derive(GraphQLInputObject, Debug, Clone)]
#[graphql(description = "A new unit")]
pub struct NewUnitGqlInput {
    pub unique_name: UnitName,
    pub translations: Vec<NewUnitTranslationNestedGqlInput>
}

impl From<Unit> for UnitGql {
    fn from(i: Unit) -> Self {
        UnitGql {
            id: i.id,
            unique_name: i.name,
            name: None,
        }
    }
}

graphql_object!(UnitGql: GraphQLContext  as "Unit" |&self| {
    field id() -> &UnitId {
        &self.id
    }

    field unique_name() -> &UnitName {
        &self.unique_name
    }

    field name(&executor) -> FieldResult<Option<UnitTranslationName>> {
        name_resolver(&self.id, executor)
    }
});