use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient::model::IngredientId;
use crate::modules::ingredient_translation::model::IngredientTranslation;
use crate::modules::ingredient_translation::model::IngredientTranslationId;
use crate::modules::ingredient_translation::model::IngredientTranslationName;
use crate::modules::language::model::LanguageId;

#[graphql(description = "An ingredient translation")]
pub struct IngredientTranslationGql {
    pub id: IngredientTranslationId,
    pub name: IngredientTranslationName,
}

#[derive(GraphQLInputObject)]
#[graphql(description = "A new ingredient translation")]
pub struct NewIngredientTranslationGqlInput {
    pub language_id: LanguageId,
    pub ingredient_id: IngredientId,
    pub name: IngredientTranslationName,
}

#[derive(GraphQLInputObject, Clone)]
#[graphql(description = "A new ingredient translation, used where ingredient_id is known")]
pub struct NewIngredientTranslationNestedGqlInput {
    pub name: IngredientTranslationName,
    pub language_id: LanguageId,
}

impl From<IngredientTranslation> for IngredientTranslationGql {
    fn from(i: IngredientTranslation) -> Self {
        IngredientTranslationGql {
            id: i.id,
            name: i.name,
        }
    }
}

graphql_object!(IngredientTranslationGql: GraphQLContext  as "IngredientTranslation" |&self| {
    field id() -> &IngredientTranslationId {
        &self.id
    }

    field name() -> &IngredientTranslationName {
        &self.name
    }
});