use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient_translation::gql::entity::IngredientTranslationGql;
use crate::modules::ingredient_translation::model::IngredientTranslation;
use crate::modules::ingredient_translation::model::IngredientTranslationId;

pub fn get_by_id_resolver(id: &IngredientTranslationId, executor: &Executor<GraphQLContext>) -> FieldResult<IngredientTranslationGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let tr = IngredientTranslation::get_by_id(id, &conn)?;

    Ok(IngredientTranslationGql::from(tr))
}
