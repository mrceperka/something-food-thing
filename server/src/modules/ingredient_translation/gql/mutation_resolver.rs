use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient_translation::gql::entity::IngredientTranslationGql;
use crate::modules::ingredient_translation::gql::entity::NewIngredientTranslationGqlInput;
use crate::modules::ingredient_translation::model::{IngredientTranslation, NewIngredientTranslation};

pub fn create_resolver(input: NewIngredientTranslationGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<IngredientTranslationGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let tr = IngredientTranslation::create(NewIngredientTranslation::from(input), &conn)?;

    Ok(IngredientTranslationGql::from(tr))
}
