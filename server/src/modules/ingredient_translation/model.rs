use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::ingredient_translation;
use crate::modules::ingredient::model::IngredientId;
use crate::modules::ingredient_translation::gql::entity::NewIngredientTranslationGqlInput;
use crate::modules::language::model::LanguageId;

// use crate::modules::ingredient::gql::entity::NewIngredientGqlInput;

pub type IngredientTranslationName = String;
pub type IngredientTranslationId = i32;

#[table_name = "ingredient_translation"]
#[derive(Serialize, Queryable, Debug, Clone)]
pub struct IngredientTranslation {
    pub id: IngredientTranslationId,
    pub language_id: LanguageId,
    pub ingredient_id: IngredientId,
    pub name: IngredientTranslationName,
}

#[table_name = "ingredient_translation"]
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
pub struct NewIngredientTranslation {
    pub language_id: LanguageId,
    pub ingredient_id: IngredientId,
    pub name: IngredientTranslationName,
}

impl From<NewIngredientTranslationGqlInput> for NewIngredientTranslation {
    fn from(input: NewIngredientTranslationGqlInput) -> Self {
        NewIngredientTranslation {
            name: input.name,
            language_id: input.language_id,
            ingredient_id: input.ingredient_id,
        }
    }
}

impl IngredientTranslation {
    pub fn get_by_id(id: &IngredientTranslationId, conn: &DbConnection) -> Result<IngredientTranslation, Error> {
        ingredient_translation::table
            .find(id)
            .first(conn)
    }

    pub fn get_by_ingredient_id_and_language_id(id: &IngredientId, lang_id: &LanguageId, conn: &DbConnection) -> Result<IngredientTranslation, Error>
    {
        ingredient_translation::table
            .filter(ingredient_translation::ingredient_id.eq(id))
            .filter(ingredient_translation::language_id.eq(lang_id))
            .first(conn)
    }

    pub fn create(item: NewIngredientTranslation, conn: &DbConnection) -> Result<IngredientTranslation, Error> {
        diesel::insert_into(ingredient_translation::table)
            .values(&item)
            .get_result(conn)
    }
}