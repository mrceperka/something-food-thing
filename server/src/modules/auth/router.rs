use rocket::{post, Route, routes};
use rocket::http::{Cookie, Cookies, Status};
use rocket_contrib::json::{Json, JsonValue};

use crate::db::pool::DbConnectionRequestGuard;
use crate::modules::user_credential::model::UserCredential;
use crate::modules::user_credential::password_bcrypt;

#[derive(Deserialize, Debug)]
struct Input {
    username: String,
    password: String,
}

#[post("/login", data = "<input>")]
fn login(
    input: Json<Input>,
    conn: DbConnectionRequestGuard,
    mut cookies: Cookies,
) -> Status {
    match UserCredential::get_by_username(&input.username, &conn) {
        Ok(u) => {
            // cca 3 secs..., cost 12
            match password_bcrypt::verify_password(&input.password, &u.password) {
                Ok(true) => {
                    cookies.add_private(Cookie::new("sid", input.username.clone()));
                    Status::Ok
                }
                _ => Status::Forbidden
            }
        }
        _ => Status::Forbidden
    }
}

#[post("/logout")]
fn logout(mut cookies: Cookies) -> JsonValue {
    cookies.remove_private(Cookie::named("sid"));
    json!({ "status": "ok"})
}

pub fn routes() -> Vec<Route> {
    routes![login, logout]
}