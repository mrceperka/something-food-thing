#[derive(GraphQLObject)]
#[graphql(description = "A login response")]
pub struct AuthByUserCredentialsGql {
    pub token: String,
}