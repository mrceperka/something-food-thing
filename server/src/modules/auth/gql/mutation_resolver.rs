use juniper::Executor;
use crate::graphql::context::GraphQLContext;
use juniper::FieldResult;
use crate::modules::auth::service::token::AuthTokenService;
use juniper::FieldError;
use crate::modules::auth::gql::entity::AuthByUserCredentialsGql;

pub fn login(username: String, password: String, executor: &Executor<GraphQLContext>) -> FieldResult<AuthByUserCredentialsGql> {
    let ctx = executor.context();
    let _conn =  ctx.db_pool.get()?;

    match AuthTokenService::login_with_username_and_password(username, password) {
        Ok(token) => Ok(AuthByUserCredentialsGql{token}),
        Err(e) => Err(FieldError::from(e))
    }
}
