use paseto;
use rocket::Outcome;
use rocket::Request;
use rocket::request;
use rocket::request::FromRequest;
use serde_json::Value;
use crate::modules::auth::enums::token::AuthTokenError;
use crate::config::CONFIG;
#[derive(Debug)]
pub struct AuthToken(Result<Value, AuthTokenError>);


impl<'a, 'r> FromRequest<'a, 'r> for AuthToken {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<AuthToken, ()> {
        let keys: Vec<_> = request.headers().get("x-api-key").collect();
        if keys.len() != 1 {
            return Outcome::Success(AuthToken(Err(AuthTokenError::MissingHeader)));
        }

        let token = keys[0];

        match paseto::tokens::validate_local_token(
            token.to_string(),
            None,
            Vec::from(CONFIG.paseto_secret.as_bytes()),
        ) {
            Ok(t) => Outcome::Success(AuthToken(Ok(t))),
            x => {
                println!("{:?}", x);
                Outcome::Success(AuthToken(Err(AuthTokenError::InvalidSignature)))
            }
        }
    }
}
