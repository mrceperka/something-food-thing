#[derive(Debug)]
pub enum AuthTokenError {
    MissingHeader,
    InvalidSignature
}