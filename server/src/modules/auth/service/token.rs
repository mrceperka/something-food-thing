use chrono::prelude::*;
use paseto;
use crate::config::CONFIG;
pub struct AuthTokenService;

impl AuthTokenService {
    pub fn login_with_username_and_password(_username: String, _password: String) -> Result<String, failure::Error> {
        let current_date_time = Utc::now();
        let dt = Utc.ymd(current_date_time.year() + 1, 7, 8).and_hms(9, 10, 11);

        let token = paseto::tokens::PasetoBuilder::new()
            .set_encryption_key(Vec::from(CONFIG.paseto_secret.as_bytes()))
            .set_issued_at(None)
            .set_expiration(dt)
            .set_issuer(String::from("something-food"))
            .set_not_before(Utc::now())
            .set_subject(String::from("some-token"))
            .build();
        println!("{:?}", token);
        token
    }
}