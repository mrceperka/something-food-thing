use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::unit_translation;
use crate::modules::language::model::LanguageId;
use crate::modules::unit::model::UnitId;
use crate::modules::unit_translation::gql::entity::NewUnitTranslationGqlInput;

pub type UnitTranslationName = String;
pub type UnitTranslationId = i32;

#[table_name = "unit_translation"]
#[derive(Serialize, Queryable, Debug, Clone)]
pub struct UnitTranslation {
    pub id: UnitTranslationId,
    pub language_id: LanguageId,
    pub unit_id: UnitId,
    pub name: UnitTranslationName,
}

#[table_name = "unit_translation"]
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
pub struct NewUnitTranslation {
    pub language_id: LanguageId,
    pub unit_id: UnitId,
    pub name: UnitTranslationName,
}

impl From<NewUnitTranslationGqlInput> for NewUnitTranslation {
    fn from(input: NewUnitTranslationGqlInput) -> Self {
        NewUnitTranslation {
            name: input.name,
            language_id: input.language_id,
            unit_id: input.unit_id,
        }
    }
}

impl UnitTranslation {
    pub fn get_by_id(id: &UnitTranslationId, conn: &DbConnection) -> Result<UnitTranslation, Error> {
        unit_translation::table
            .find(id)
            .first(conn)
    }

    pub fn get_by_unit_id_and_language_id(id: &UnitId, lang_id: &LanguageId, conn: &DbConnection) -> Result<UnitTranslation, Error>
    {
        unit_translation::table
            .filter(unit_translation::unit_id.eq(id))
            .filter(unit_translation::language_id.eq(lang_id))
            .first(conn)
    }

    pub fn create(item: NewUnitTranslation, conn: &DbConnection) -> Result<UnitTranslation, Error> {
        diesel::insert_into(unit_translation::table)
            .values(&item)
            .get_result(conn)
    }
}