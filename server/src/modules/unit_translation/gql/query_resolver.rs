use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::unit_translation::gql::entity::UnitTranslationGql;
use crate::modules::unit_translation::model::UnitTranslation;
use crate::modules::unit_translation::model::UnitTranslationId;

pub fn get_by_id_resolver(id: &UnitTranslationId, executor: &Executor<GraphQLContext>) -> FieldResult<UnitTranslationGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let tr = UnitTranslation::get_by_id(id, &conn)?;

    Ok(UnitTranslationGql::from(tr))
}
