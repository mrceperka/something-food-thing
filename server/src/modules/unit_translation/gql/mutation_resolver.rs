use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::unit_translation::gql::entity::UnitTranslationGql;
use crate::modules::unit_translation::gql::entity::NewUnitTranslationGqlInput;
use crate::modules::unit_translation::model::{UnitTranslation, NewUnitTranslation};

pub fn create_resolver(input: NewUnitTranslationGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<UnitTranslationGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let tr = UnitTranslation::create(NewUnitTranslation::from(input), &conn)?;

    Ok(UnitTranslationGql::from(tr))
}
