use crate::graphql::context::GraphQLContext;
use crate::modules::language::model::LanguageId;
use crate::modules::unit::model::UnitId;
use crate::modules::unit_translation::model::UnitTranslation;
use crate::modules::unit_translation::model::UnitTranslationId;
use crate::modules::unit_translation::model::UnitTranslationName;

#[graphql(description = "An unit translation")]
pub struct UnitTranslationGql {
    pub id: UnitTranslationId,
    pub name: UnitTranslationName,
}

#[derive(GraphQLInputObject)]
#[graphql(description = "A new unit translation")]
pub struct NewUnitTranslationGqlInput {
    pub language_id: LanguageId,
    pub unit_id: UnitId,
    pub name: UnitTranslationName,
}

#[derive(GraphQLInputObject, Debug, Clone)]
#[graphql(description = "A new unit translation, used where unit_id is known")]
pub struct NewUnitTranslationNestedGqlInput {
    pub name: UnitTranslationName,
    pub language_id: LanguageId,
}

impl From<UnitTranslation> for UnitTranslationGql {
    fn from(i: UnitTranslation) -> Self {
        UnitTranslationGql {
            id: i.id,
            name: i.name,
        }
    }
}

graphql_object!(UnitTranslationGql: GraphQLContext  as "UnitTranslation" |&self| {
    field id() -> &UnitTranslationId {
        &self.id
    }

    field name() -> &UnitTranslationName {
        &self.name
    }
});