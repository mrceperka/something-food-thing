use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::recipe_translation;
use crate::modules::language::model::LanguageId;
use crate::modules::recipe::model::{Recipe, RecipeId};

pub type RecipeTranslationName = String;
pub type RecipeTranslationId = i32;

#[table_name = "recipe_translation"]
#[derive(Serialize, Identifiable, Associations, Queryable, Debug, Clone)]
#[belongs_to(Recipe)]
pub struct RecipeTranslation {
    pub id: RecipeTranslationId,
    pub language_id: LanguageId,
    pub recipe_id: RecipeId,
    pub name: RecipeTranslationName,
}

#[table_name = "recipe_translation"]
#[derive(Serialize, Deserialize, Insertable)]
pub struct NewRecipeTranslation {
    pub language_id: LanguageId,
    pub recipe_id: RecipeId,
    pub name: RecipeTranslationName,
}

impl RecipeTranslation {
    pub fn get_by_recipe_id_and_language_id(recipe_id: &RecipeId, lang_id: &LanguageId, conn: &DbConnection) -> Result<RecipeTranslation, Error> {
        recipe_translation::table
            .filter(recipe_translation::recipe_id.eq(recipe_id))
            .filter(recipe_translation::language_id.eq(lang_id))
            .first(conn)
    }
    pub fn create(item: NewRecipeTranslation, conn: &DbConnection) -> Result<RecipeTranslation, Error> {
        diesel::insert_into(recipe_translation::table)
            .values(&item)
            .get_result(conn)
    }
}