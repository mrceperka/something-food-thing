use diesel::expression::dsl::not;
use diesel::expression::exists::exists;
use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::recipe;
use crate::db::schema::recipe_ingredient_view;
use crate::db::schema::recipe_translation;
use crate::modules::language::model::LanguageId;
use crate::modules::recipe::enums::difficulty::RecipeDifficulty;
use crate::modules::recipe::gql::entity::RecipeFilterGqlInput;

pub type RecipeId = i32;


/*
table! {
    use diesel::sql_types::*;
    use crate::modules::recipe::enums::difficulty::RecipeDifficultyType;
    recipe (id) {
        id -> Int4,
        difficulty -> RecipeDifficultyType,
    }
}
*/

#[table_name = "recipe"]
#[derive(Identifiable, Serialize, Queryable, Debug, Clone)]
pub struct Recipe {
    pub id: RecipeId,
    pub difficulty: RecipeDifficulty,
}

#[table_name = "recipe"]
#[derive(Serialize, Deserialize, Insertable)]
pub struct NewRecipe {
    pub difficulty: RecipeDifficulty,
}

impl Recipe {
    pub fn get_by_id(id: &RecipeId, conn: &DbConnection) -> Result<Recipe, Error> {
        recipe::table
            .find(id)
            .first(conn)
    }
    pub fn get_by_id_and_language_id(id: RecipeId, lang_id: LanguageId, conn: &DbConnection) -> Result<(Recipe, Option<String>), Error> {
        recipe::table
            .find(id)
            .left_join(recipe_translation::table.on(recipe_translation::language_id.eq(lang_id)))
            .select((recipe::all_columns, recipe_translation::name.nullable()))
            .order(recipe_translation::name.asc())
            .first(conn)
    }

    pub fn create(r: NewRecipe, conn: &DbConnection) -> Result<Recipe, Error> {
        diesel::insert_into(recipe::table)
            .values(&r)
            .get_result(conn)
    }

    pub fn filter(filter: &RecipeFilterGqlInput, conn: &DbConnection) -> Result<Vec<Recipe>, Error> {
        let mut query = recipe::table
            .select(recipe::all_columns)
            .into_boxed();


        if let Some(d) = &filter.difficulty {
            query = query.filter(
                recipe::difficulty.eq(d)
            );
        }

        if let Some(contain) = &filter.contain_ingredients {
            let sub = recipe_ingredient_view::table
                .filter(recipe_ingredient_view::recipe_id.eq(recipe::id))
                .filter(recipe_ingredient_view::ingredient_id.eq_any(contain))
                .select(recipe_ingredient_view::ingredient_id);
            query = query.filter(
                exists(sub)
            );
        }

        if let Some(not_contain) = &filter.not_contain_ingredients {
            let sub = recipe_ingredient_view::table
                .filter(recipe_ingredient_view::recipe_id.eq(recipe::id))
                .filter(recipe_ingredient_view::ingredient_id.eq_any(not_contain))
                .select(recipe_ingredient_view::ingredient_id);
            query = query.filter(
                not(exists(sub))
            );
        }

        if let Some(name) = &filter.name {
            let subselect = recipe_translation::table
                .select(recipe_translation::recipe_id)
                .filter(recipe_translation::recipe_id.eq(recipe::id))
                .filter(recipe_translation::language_id.eq(1))
                .filter(recipe_translation::name.like(format!("%{}%", name)));


            query = query.filter(
                recipe::id.eq_any(subselect)
            );
        }

        query.load(conn)
    }
}