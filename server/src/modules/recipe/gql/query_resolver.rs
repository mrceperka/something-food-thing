use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::recipe::gql::entity::RecipeFilterGqlInput;
use crate::modules::recipe::gql::entity::RecipeGql;
use crate::modules::recipe::model::{Recipe, RecipeId};
use crate::modules::recipe_preparation_step::gql::entity::RecipePreparationStepGql;
use crate::modules::recipe_preparation_step::model::RecipePreparationStep;
use crate::modules::recipe_translation::model::{RecipeTranslation, RecipeTranslationName};
use crate::modules::language::model::LanguageId;

pub fn name_resolver(id: &RecipeId, language_id_opt: &Option<LanguageId>, executor: &Executor<GraphQLContext>) -> FieldResult<Option<RecipeTranslationName>> {
    let ctx = executor.context();
    let conn = &ctx.db_pool.get()?;
    let lid = match language_id_opt {
        Some(v) => v,
        _ => &ctx.language.id
    };
    match RecipeTranslation::get_by_recipe_id_and_language_id(id, lid, conn) {
        Ok(tr) => Ok(Some(tr.name)),
        _ => Ok(None)
    }
}

pub fn get_by_id_resolver(id: &RecipeId, executor: &Executor<GraphQLContext>) -> FieldResult<RecipeGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let recipe = Recipe::get_by_id(id, &conn)?;

    Ok(RecipeGql::from(recipe))
}

pub fn preparation_steps_resolver(id: &RecipeId, executor: &Executor<GraphQLContext>) -> Vec<RecipePreparationStepGql> {
    let ctx = executor.context();
    let conn = match ctx.db_pool.get() {
        Ok(c) => c,
        _ => return vec![]
    };

    RecipePreparationStep::find_by_recipe_id(id, &conn)
        .unwrap_or(vec![])
        .into_iter()
        .map(|rps| RecipePreparationStepGql::from(rps))
        .collect::<Vec<RecipePreparationStepGql>>()
}


pub fn filter_recipes_resolver(input: &RecipeFilterGqlInput, executor: &Executor<GraphQLContext>) -> Vec<RecipeGql> {
    let ctx = executor.context();
    let conn = match ctx.db_pool.get() {
        Ok(c) => c,
        _ => return vec![]
    };

    Recipe::filter(&input, &conn)
        .unwrap_or(vec![])
        .into_iter()
        .map(|rps| RecipeGql::from(rps))
        .collect::<Vec<RecipeGql>>()
}