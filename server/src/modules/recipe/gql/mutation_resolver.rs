use diesel::prelude::*;
use diesel::result::Error;
use juniper::Executor;
use juniper::FieldError;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::recipe::gql::entity::NewRecipeGqlInput;
use crate::modules::recipe::gql::entity::RecipeGql;
use crate::modules::recipe::model::{NewRecipe, Recipe};
use crate::modules::recipe_preparation_step::model::{NewRecipePreparationStep, RecipePreparationStep};
use crate::modules::recipe_preparation_step_ingredient::model::{NewRecipePreparationStepIngredient, RecipePreparationStepIngredient};
use crate::modules::recipe_translation::model::NewRecipeTranslation;
use crate::modules::recipe_translation::model::RecipeTranslation;

pub fn create_resolver(input: NewRecipeGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<RecipeGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    match conn.transaction::<Recipe, Error, _>(|| {
        let new_recipe = NewRecipe {
            difficulty: input.difficulty
        };
        let r = Recipe::create(new_recipe, &conn)?;

        let new_recipe_translation = NewRecipeTranslation {
            language_id: input.language_id,
            recipe_id: r.id,
            name: input.name,
        };
        RecipeTranslation::create(new_recipe_translation, &conn)?;

        for ps in input.preparation_steps {
            let new_rps = NewRecipePreparationStep {
                recipe_id: r.id,
                position: ps.position,
            };
            let rps = RecipePreparationStep::create(new_rps, &conn)?;
            for rpsi in ps.ingredients {
                RecipePreparationStepIngredient::create(NewRecipePreparationStepIngredient {
                    recipe_preparation_step_id: rps.id,
                    ingredient_id: rpsi.ingredient_id,
                    unit_id: rpsi.unit_id,
                    amount: rpsi.amount,
                }, &conn)?;
            }
        }
        Ok(r)
    }) {
        Ok(r) => Ok(RecipeGql::from(r)),
        _ => Err(FieldError::from("Db failure"))
    }
}
