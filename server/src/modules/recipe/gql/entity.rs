use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient::model::IngredientId;
use crate::modules::language::model::LanguageId;
use crate::modules::recipe::enums::difficulty::RecipeDifficulty;
use crate::modules::recipe::gql::query_resolver::{name_resolver, preparation_steps_resolver};
use crate::modules::recipe::model::Recipe;
use crate::modules::recipe::model::RecipeId;
use crate::modules::recipe_preparation_step::gql::entity::NewRecipePreparationStepNestedGqlInput;
use crate::modules::recipe_preparation_step::gql::entity::RecipePreparationStepGql;
use crate::modules::recipe_translation::model::RecipeTranslationName;

#[graphql(description = "A recipe")]
pub struct RecipeGql {
    pub id: RecipeId,
    pub name: Option<RecipeTranslationName>,
    pub difficulty: RecipeDifficulty,
    pub preparation_steps: Vec<RecipePreparationStepGql>,

    // for translations
    pub language_id: Option<LanguageId>,
}


#[derive(GraphQLInputObject)]
#[graphql(description = "A new recipe")]
pub struct NewRecipeGqlInput {
    pub name: String,
    pub language_id: LanguageId,
    pub difficulty: RecipeDifficulty,
    pub preparation_steps: Vec<NewRecipePreparationStepNestedGqlInput>,
}

#[derive(GraphQLInputObject, Debug)]
#[graphql(description = "A complex search for recipes")]
pub struct RecipeFilterGqlInput {
    pub name: Option<RecipeTranslationName>,
    // skip language_id for now

    pub contain_ingredients: Option<Vec<IngredientId>>,
    pub not_contain_ingredients: Option<Vec<IngredientId>>,
    pub difficulty: Option<RecipeDifficulty>,
}

impl From<Recipe> for RecipeGql {
    fn from(r: Recipe) -> Self {
        RecipeGql {
            id: r.id,
            difficulty: r.difficulty,
            name: None,
            preparation_steps: vec![],
            language_id: None
        }
    }
}

graphql_object!(RecipeGql: GraphQLContext  as "Recipe" |&self| {
    field id() -> &RecipeId {
        &self.id
    }

    field difficulty() -> &RecipeDifficulty {
        &self.difficulty
    }

    field name(&executor) -> FieldResult<Option<RecipeTranslationName>> {
        name_resolver(&self.id, &self.language_id, executor)
    }

    field preparation_steps(&executor) -> Vec<RecipePreparationStepGql> {
       preparation_steps_resolver(&self.id, executor)
    }
});