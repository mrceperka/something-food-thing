use std::io::Write;

use diesel::deserialize as diesel_des;
use diesel::serialize as diesel_ser;

use crate::db::pool::DbBackend;

#[derive(SqlType)]
#[postgres(type_name = "recipe_difficulty")]
pub struct RecipeDifficultyType;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, FromSqlRow, AsExpression)]
#[sql_type = "RecipeDifficultyType"]
#[serde(rename_all = "kebab-case")]
#[derive(GraphQLEnum)]
pub enum RecipeDifficulty {
    #[graphql(name="easy")]
    Easy,
    #[graphql(name="medium")]
    Medium,
    #[graphql(name="hard")]
    Hard,
}

impl diesel_ser::ToSql<RecipeDifficultyType, DbBackend> for RecipeDifficulty {
    fn to_sql<W: Write>(&self, out: &mut diesel_ser::Output<W, DbBackend>) -> diesel_ser::Result {
        match *self {
            RecipeDifficulty::Easy => out.write_all(b"easy")?,
            RecipeDifficulty::Medium => out.write_all(b"medium")?,
            RecipeDifficulty::Hard => out.write_all(b"hard")?,
        }
        Ok(diesel_ser::IsNull::No)
    }
}

impl diesel_des::FromSql<RecipeDifficultyType, DbBackend> for RecipeDifficulty {
    fn from_sql(bytes: Option<&[u8]>) -> diesel_des::Result<Self> {
        match not_none!(bytes) {
            b"easy" => Ok(RecipeDifficulty::Easy),
            b"medium" => Ok(RecipeDifficulty::Medium),
            b"hard" => Ok(RecipeDifficulty::Hard),
            _ => Err("Unrecognized recipe_difficulty enum variant".into()),
        }
    }
}