use chrono::{DateTime, Utc};
use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::user;

pub type UserId = i32;
pub type UserCreatedAt = DateTime<Utc>;

#[table_name = "user"]
#[derive(Serialize, Queryable, Debug, Clone)]
pub struct User {
    pub id: UserId,
    pub created_at: UserCreatedAt,
}

#[table_name = "user"]
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
pub struct NewUser {
    pub created_at: UserCreatedAt,
}

impl User {
    pub fn get_by_id(id: &UserId, conn: &DbConnection) -> Result<User, Error> {
        user::table
            .find(id)
            .first(conn)
    }


    pub fn create(item: NewUser, conn: &DbConnection) -> Result<User, Error> {
        diesel::insert_into(user::table)
            .values(&item)
            .get_result(conn)
    }
}