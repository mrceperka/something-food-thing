use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::user::gql::query_resolver::credential_resolver;
use crate::modules::user::model::User;
use crate::modules::user::model::UserCreatedAt;
use crate::modules::user::model::UserId;
use crate::modules::user_credential::gql::entity::UserCredentialGql;
use crate::modules::user_credential::model::UserCredentialPassword;
use crate::modules::user_credential::model::UserCredentialUsername;

#[graphql(description = "A user")]
pub struct UserGql {
    pub id: UserId,
    pub created_at: UserCreatedAt,
    pub credential: Box<Option<UserCredentialGql>>,
}

impl From<User> for UserGql {
    fn from(u: User) -> Self {
        UserGql {
            id: u.id,
            created_at: u.created_at,
            credential: Box::new(None),
        }
    }
}

#[derive(GraphQLInputObject)]
#[graphql(description = "A new user")]
pub struct NewUserGqlInput {
    pub username: UserCredentialUsername,
    pub password: UserCredentialPassword,
}

graphql_object!(UserGql: GraphQLContext  as "User" |&self| {
    field id() -> &UserId {
        &self.id
    }

    field created_at() -> &UserCreatedAt {
        &self.created_at
    }

    field credential(&executor) -> FieldResult<UserCredentialGql> {
        credential_resolver(&self.id, executor)
    }
});