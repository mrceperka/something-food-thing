use diesel::prelude::*;
use diesel::result::Error;
use juniper::{Executor, FieldError, FieldResult, Value};

use crate::graphql::context::GraphQLContext;
use crate::modules::user::gql::entity::NewUserGqlInput;
use crate::modules::user::gql::entity::UserGql;
use crate::modules::user::model::NewUser;
use crate::modules::user::model::User;
use crate::modules::user_credential::model::NewUserCredential;
use crate::modules::user_credential::model::UserCredential;
use crate::modules::user_credential::password_bcrypt;
use crate::modules::user_credential::password_validator;

pub fn create_resolver(input: NewUserGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<UserGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;

    match password_validator::is_bcrypt_valid(&input.password.as_str()) {
        Err(e) => return Err(FieldError::new(e, Value::scalar(
            "password_error".to_string()
        ))),
        _ => {}
    };
    match conn.transaction::<User, Error, _>(|| {
        let user = User::create(
            NewUser {
                created_at: chrono::Utc::now()
            },
            &conn,
        )?;

        let hash = match password_bcrypt::hash_password(&input.password) {
            Ok(h) => h,
            Err(e) => {
                println!("{:?}", e);
                return Err(Error::RollbackTransaction);
            }
        };

        UserCredential::create(
            NewUserCredential {
                user_id: user.id,
                username: input.username,
                password: hash,
            },
            &conn,
        )?;

        Ok(user)
    }) {
        Ok(r) => Ok(UserGql::from(r)),
        Err(e) => Err(FieldError::from(e))
    }
}
