use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::user::gql::entity::UserGql;
use crate::modules::user::model::User;
use crate::modules::user::model::UserId;
use crate::modules::user_credential::model::UserCredential;
use crate::modules::user_credential::gql::entity::UserCredentialGql;

pub fn get_by_id_resolver(id: &UserId, executor: &Executor<GraphQLContext>) -> FieldResult<UserGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;

    let u = User::get_by_id(id, &conn)?;
    Ok(UserGql::from(u))
}

pub fn credential_resolver(id: &UserId, executor: &Executor<GraphQLContext>) -> FieldResult<UserCredentialGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;

    let u = UserCredential::get_by_user_id(id, &conn)?;
    Ok(UserCredentialGql::from(u))
}