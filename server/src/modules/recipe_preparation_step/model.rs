use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::recipe_preparation_step;
use crate::db::schema::recipe_preparation_step_translation;
use crate::modules::language::model::LanguageId;
use crate::modules::recipe::model::{Recipe, RecipeId};
use crate::modules::recipe_preparation_step_translation::model::RecipePreparationStepTranslation;
use crate::modules::recipe_preparation_step::gql::entity::NewRecipePreparationStepGqlInput;

pub type RecipePreparationStepPosition = i32;
pub type RecipePreparationStepId = i32;

#[table_name = "recipe_preparation_step"]
#[derive(Serialize, Identifiable, Associations, Queryable, Debug, Clone)]
#[belongs_to(Recipe)]
pub struct RecipePreparationStep {
    pub id: RecipePreparationStepId,
    pub position: RecipePreparationStepPosition,
    pub recipe_id: RecipeId,
}

#[table_name = "recipe_preparation_step"]
#[derive(Serialize, Deserialize, Insertable)]
pub struct NewRecipePreparationStep {
    pub position: RecipePreparationStepPosition,
    pub recipe_id: RecipeId,
}

impl From<&NewRecipePreparationStepGqlInput> for NewRecipePreparationStep {
    fn from(v: &NewRecipePreparationStepGqlInput)-> Self {
        NewRecipePreparationStep {
            position: v.position,
            recipe_id: v.recipe_id
        }
    }
}

impl RecipePreparationStep {
    pub fn create(item: NewRecipePreparationStep, conn: &DbConnection) -> Result<RecipePreparationStep, Error> {
        diesel::insert_into(recipe_preparation_step::table)
            .values(&item)
            .get_result(conn)
    }

    pub fn find_by_recipe_id(id: &RecipeId, conn: &DbConnection) -> Result<Vec<RecipePreparationStep>, Error> {
        recipe_preparation_step::table
            .filter(recipe_preparation_step::recipe_id.eq(id))
            .load(conn)
    }

    pub fn find_by_recipe_id_and_language_id(id: &RecipeId, lang_id: &LanguageId, conn: &DbConnection) -> Result<Vec<(RecipePreparationStep, Option<RecipePreparationStepTranslation>)>, Error> {
        recipe_preparation_step::table
            .filter(recipe_preparation_step::recipe_id.eq(id))
            .left_join(recipe_preparation_step_translation::table.on(recipe_preparation_step_translation::language_id.eq(lang_id)))
            .select((recipe_preparation_step::all_columns, recipe_preparation_step_translation::all_columns.nullable()))
            .order(recipe_preparation_step_translation::description.asc())
            .load(conn)
    }

    pub fn get_by_id(id: &RecipePreparationStepId, conn: &DbConnection) -> Result<RecipePreparationStep, Error> {
        recipe_preparation_step::table
            .find(id)
            .first(conn)
    }
}