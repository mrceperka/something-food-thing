use diesel::prelude::*;
use diesel::result::Error;
use juniper::Executor;
use juniper::FieldError;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::language::model::Language;
use crate::modules::recipe_preparation_step::gql::entity::{NewRecipePreparationStepGqlInput, RecipePreparationStepGql};
use crate::modules::recipe_preparation_step::model::{NewRecipePreparationStep, RecipePreparationStep};
use crate::modules::recipe_preparation_step_translation::model::{NewRecipePreparationStepTranslation, RecipePreparationStepTranslation};
use crate::modules::recipe_preparation_step_ingredient::model::RecipePreparationStepIngredient;
use crate::modules::recipe_preparation_step_ingredient::model::NewRecipePreparationStepIngredient;

pub fn create_resolver(input: NewRecipePreparationStepGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<RecipePreparationStepGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let lang = Language::get_by_code(&input.language, &conn)?;
    match conn.transaction::<RecipePreparationStep, Error, _>(|| {
        let rps = RecipePreparationStep::create(NewRecipePreparationStep::from(&input), &conn)?;
        RecipePreparationStepTranslation::create(
            NewRecipePreparationStepTranslation {
                recipe_preparation_step_id: rps.id,
                description: input.description,
                language_id: lang.id,
            }, &conn)?;

        for v in input.ingredients {
            RecipePreparationStepIngredient::create(
                NewRecipePreparationStepIngredient {
                    recipe_preparation_step_id: rps.id,
                    unit_id: v.unit_id,
                    ingredient_id: v.ingredient_id,
                    amount: v.amount,
                },
                &conn,
            )?;
        }
        Ok(rps)
    }) {
        Ok(r) => Ok(RecipePreparationStepGql::from(r)),
        _ => Err(FieldError::from("Db failure"))
    }
}
