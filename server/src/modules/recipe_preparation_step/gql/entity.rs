use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::language::model::LanguageCode;
use crate::modules::recipe::gql::entity::RecipeGql;
use crate::modules::recipe::model::RecipeId;
use crate::modules::recipe_preparation_step::gql::query_resolver::{description_resolver, recipe_resolver, ingredients_resolver};
use crate::modules::recipe_preparation_step::model::RecipePreparationStep;
use crate::modules::recipe_preparation_step::model::RecipePreparationStepId;
use crate::modules::recipe_preparation_step::model::RecipePreparationStepPosition;
use crate::modules::recipe_preparation_step_translation::model::RecipePreparationStepTranslationDescription;
use crate::modules::recipe_preparation_step_ingredient::gql::entity::RecipePreparationStepIngredientGql;
use crate::modules::recipe_preparation_step_ingredient::gql::entity::NewRecipePreparationStepIngredientNestedGqlInput;

#[graphql(description = "A recipe preparation step")]
pub struct RecipePreparationStepGql {
    pub id: RecipePreparationStepId,
    pub recipe_id: RecipeId,
    pub position: RecipePreparationStepPosition,
    pub description: RecipePreparationStepTranslationDescription,
    pub ingredients: Vec<RecipePreparationStepIngredientGql>
}

#[derive(GraphQLInputObject)]
#[graphql(description = "A new recipe preparation step")]
pub struct NewRecipePreparationStepGqlInput {
    pub recipe_id: RecipeId,
    pub position: RecipePreparationStepPosition,
    pub description: RecipePreparationStepTranslationDescription,
    pub language: LanguageCode,
    pub ingredients: Vec<NewRecipePreparationStepIngredientNestedGqlInput>
}


#[derive(GraphQLInputObject)]
#[graphql(description = "A new recipe preparation step, used where recipe_id and language is known")]
pub struct NewRecipePreparationStepNestedGqlInput {
    pub position: RecipePreparationStepPosition,
    pub description: RecipePreparationStepTranslationDescription,
    pub ingredients: Vec<NewRecipePreparationStepIngredientNestedGqlInput>
}

impl From<RecipePreparationStep> for RecipePreparationStepGql {
    fn from(rps: RecipePreparationStep) -> Self {
        RecipePreparationStepGql {
            id: rps.id,
            recipe_id: rps.recipe_id,
            position: rps.position,
            description: String::from(""),
            ingredients: vec![]
        }
    }
}

graphql_object!(RecipePreparationStepGql: GraphQLContext as "RecipePreparationStep" |&self| {
    field id() -> &RecipePreparationStepId {
        &self.id
    }

    field position() -> &RecipePreparationStepPosition {
        &self.position
    }

    field description(&executor) -> FieldResult<Option<RecipePreparationStepTranslationDescription>> {
       description_resolver(&self.id, executor)
    }

    field recipe(&executor) -> FieldResult<RecipeGql> {
       recipe_resolver(&self.recipe_id, executor)
    }

    field ingredients(&executor) -> FieldResult<Vec<RecipePreparationStepIngredientGql>> {
       ingredients_resolver(&self.id, executor)
    }
});