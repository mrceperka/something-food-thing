use juniper::{Executor, FieldError, FieldResult};

use crate::graphql::context::GraphQLContext;
use crate::modules::recipe::gql::entity::RecipeGql;
use crate::modules::recipe::gql::query_resolver::get_by_id_resolver as get_recipe;
use crate::modules::recipe::model::RecipeId;
use crate::modules::recipe_preparation_step::gql::entity::RecipePreparationStepGql;
use crate::modules::recipe_preparation_step::model::RecipePreparationStep;
use crate::modules::recipe_preparation_step::model::RecipePreparationStepId;
use crate::modules::recipe_preparation_step_translation::model::{RecipePreparationStepTranslation, RecipePreparationStepTranslationDescription};
use crate::modules::recipe_preparation_step_ingredient::model::RecipePreparationStepIngredient;
use crate::modules::recipe_preparation_step_ingredient::gql::entity::RecipePreparationStepIngredientGql;

pub fn get_by_id_resolver(id: &RecipePreparationStepId, executor: &Executor<GraphQLContext>) -> FieldResult<RecipePreparationStepGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let rps = RecipePreparationStep::get_by_id(id, &conn)?;

    Ok(RecipePreparationStepGql::from(rps))
}


pub fn description_resolver(id: &RecipePreparationStepId, executor: &Executor<GraphQLContext>) -> FieldResult<Option<RecipePreparationStepTranslationDescription>> {
    let ctx = executor.context();
    let lang = &ctx.language;
    let conn = ctx.db_pool.get()?;
    match RecipePreparationStepTranslation::get_by_recipe_preparation_step_id_and_language_id(id, &lang.id, &conn) {
        Ok(tr) =>  Ok(Some(tr.description)),
        _ =>  Ok(None)
    }
}

pub fn recipe_resolver(id: &RecipeId, executor: &Executor<GraphQLContext>) -> FieldResult<RecipeGql> {
    get_recipe(id, executor)
}


pub fn ingredients_resolver(id: &RecipePreparationStepId, executor: &Executor<GraphQLContext>) -> FieldResult<Vec<RecipePreparationStepIngredientGql>> {
    let ctx = executor.context();
    let conn = match ctx.db_pool.get() {
        Ok(c) => c,
        _ => return Err(
            FieldError::from(
                String::from("Db failure")
            )
        )
    };
    match RecipePreparationStepIngredient::find_by_recipe_preparation_step_id(id, &conn) {
        Ok(v) => Ok(RecipePreparationStepIngredientGql::from_model_vec(v)),
        _ => Err(
            FieldError::from(
                String::from("Db failure")
            )
        )
    }
}

