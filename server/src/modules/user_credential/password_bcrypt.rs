use bcrypt::{BcryptError, DEFAULT_COST, hash, verify};

use crate::modules::user_credential::model::UserCredentialPassword;

#[derive(Debug)]
pub enum Error {
    HashingFailed(BcryptError),
}


pub fn hash_password(password: &UserCredentialPassword) -> Result<String, Error> {
    hash(password, DEFAULT_COST).map_err(|e| Error::HashingFailed(e))
}

pub fn verify_password(actual_password: &UserCredentialPassword, hashed_password: &UserCredentialPassword) -> Result<bool, Error> {
    verify(actual_password, hashed_password)
        .map_err(|e| Error::HashingFailed(e))
}