use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::user_credential::gql::entity::UserCredentialGql;
use crate::modules::user_credential::model::UserCredential;
use crate::modules::user_credential::model::UserCredentialId;

pub fn get_by_id_resolver(id: &UserCredentialId, executor: &Executor<GraphQLContext>) -> FieldResult<UserCredentialGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let unit = UserCredential::get_by_id(id, &conn)?;

    Ok(UserCredentialGql::from(unit))
}
