use crate::graphql::context::GraphQLContext;
use crate::modules::user::gql::entity::UserGql;
use crate::modules::user::model::UserId;
use crate::modules::user_credential::model::UserCredential;
use crate::modules::user_credential::model::UserCredentialId;
use crate::modules::user_credential::model::UserCredentialUsername;

#[graphql(description = "An user credential - username and password based")]
pub struct UserCredentialGql {
    pub id: UserCredentialId,
    pub user_id: UserId,
    pub username: UserCredentialUsername,
    pub user: Box<Option<UserGql>>,
}

impl From<UserCredential> for UserCredentialGql {
    fn from(i: UserCredential) -> Self {
        UserCredentialGql {
            id: i.id,
            username: i.username,
            user_id: i.user_id,
            user: Box::new(None),
        }
    }
}

graphql_object!(UserCredentialGql: GraphQLContext  as "UserCredential" |&self| {
    field id() -> &UserCredentialId {
        &self.id
    }

    field username() -> &UserCredentialUsername {
        &self.username
    }
});