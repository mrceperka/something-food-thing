use failure_derive::*;

#[derive(Debug, Fail)]
pub enum PasswordError {
    #[fail(display = "Password is too long, max is 255")]
    PasswordTooLong
}


pub fn is_bcrypt_valid(password: &str) -> Result<(), PasswordError> {

    // https://en.wikipedia.org/wiki/Bcrypt

    // $2b$ (February 2014)
    // A bug was discovered in the OpenBSD implementation of bcrypt. They were storing the length of their strings in an unsigned char (i.e. 8-bit Byte).[15] If a password was longer than 255 characters, it would overflow and wrap at 255.[17]
    // BCrypt was created for OpenBSD. When they had a bug in their library, they decided to bump the version number.
    match password.len() > 255 {
        true => Err(PasswordError::PasswordTooLong),
        _ => Ok(())
    }
}