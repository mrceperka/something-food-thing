use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::user_credential;
use crate::modules::user::model::UserId;

pub type UserCredentialId = i32;
pub type UserCredentialUsername = String;
pub type UserCredentialPassword = String;


#[table_name = "user_credential"]
#[derive(Serialize, Queryable, Debug, Clone)]
pub struct UserCredential {
    pub id: UserCredentialId,
    pub user_id: UserId,
    pub username: UserCredentialUsername,
    pub password: UserCredentialPassword,
}


#[table_name = "user_credential"]
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
pub struct NewUserCredential {
    pub user_id: UserId,
    pub username: UserCredentialUsername,
    pub password: UserCredentialPassword,
}

impl UserCredential {
    pub fn get_by_user_id(id: &UserId, conn: &DbConnection) -> Result<UserCredential, Error> {
        user_credential::table
            .filter(user_credential::user_id.eq(id))
            .first(conn)
    }

    pub fn get_by_username(username: &UserCredentialUsername, conn: &DbConnection) -> Result<UserCredential, Error> {
        user_credential::table
            .filter(user_credential::username.eq(username))
            .first(conn)
    }

    pub fn get_by_id(id: &UserCredentialId, conn: &DbConnection) -> Result<UserCredential, Error> {
        user_credential::table
            .find(id)
            .first(conn)
    }

    pub fn create(item: NewUserCredential, conn: &DbConnection) -> Result<UserCredential, Error> {
        diesel::insert_into(user_credential::table)
            .values(&item)
            .get_result(conn)
    }
}