use juniper::{Executor, FieldResult};

use crate::graphql::context::GraphQLContext;
use crate::modules::recipe_preparation_step::gql::entity::RecipePreparationStepGql;
use crate::modules::recipe_preparation_step::gql::query_resolver::get_by_id_resolver as get_recipe_preparation_step;
use crate::modules::recipe_preparation_step::model::RecipePreparationStepId;
use crate::modules::recipe_preparation_step_ingredient::gql::entity::RecipePreparationStepIngredientGql;
use crate::modules::recipe_preparation_step_ingredient::model::{RecipePreparationStepIngredient, RecipePreparationStepIngredientId};
use crate::modules::unit::gql::entity::UnitGql;
use crate::modules::unit::gql::query_resolver::get_by_id_resolver as get_unit;
use crate::modules::unit::model::UnitId;

pub fn get_by_id_resolver(id: &RecipePreparationStepIngredientId, executor: &Executor<GraphQLContext>) -> FieldResult<RecipePreparationStepIngredientGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let rpsi = RecipePreparationStepIngredient::get_by_id(id, &conn)?;

    Ok(RecipePreparationStepIngredientGql::from(rpsi))
}

pub fn recipe_preparation_step_resolver(id: &RecipePreparationStepId, executor: &Executor<GraphQLContext>) -> FieldResult<RecipePreparationStepGql> {
    get_recipe_preparation_step(id, executor)
}

pub fn unit_resolver(id: &UnitId, executor: &Executor<GraphQLContext>) -> FieldResult<UnitGql> {
    get_unit(id, executor)
}



