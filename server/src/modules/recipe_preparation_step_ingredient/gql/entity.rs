use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient::model::IngredientId;
use crate::modules::recipe_preparation_step::gql::entity::RecipePreparationStepGql;
use crate::modules::recipe_preparation_step::model::RecipePreparationStep;
use crate::modules::recipe_preparation_step::model::RecipePreparationStepId;
use crate::modules::recipe_preparation_step_ingredient::gql::query_resolver::recipe_preparation_step_resolver;
use crate::modules::recipe_preparation_step_ingredient::gql::query_resolver::unit_resolver;
use crate::modules::recipe_preparation_step_ingredient::model::RecipePreparationStepIngredient;
use crate::modules::recipe_preparation_step_ingredient::model::RecipePreparationStepIngredientAmount;
use crate::modules::recipe_preparation_step_ingredient::model::RecipePreparationStepIngredientId;
use crate::modules::unit::gql::entity::UnitGql;
use crate::modules::unit::model::Unit;
use crate::modules::unit::model::UnitId;

#[graphql(description = "A recipe preparation step ingredient")]
pub struct RecipePreparationStepIngredientGql {
    pub id: RecipePreparationStepIngredientId,
    pub amount: RecipePreparationStepIngredientAmount,
    pub ingredient_id: IngredientId,

    pub recipe_preparation_step_id: RecipePreparationStepId,
    pub recipe_preparation_step: Option<RecipePreparationStep>,

    pub unit_id: UnitId,
    pub unit: Option<Unit>,
}

#[derive(GraphQLInputObject)]
#[graphql(description = "A new recipe preparation step ingredient")]
pub struct NewRecipePreparationStepIngredientGqlInput {
    pub recipe_preparation_step_id: RecipePreparationStepId,
    pub ingredient_id: IngredientId,
    pub unit_id: UnitId,
    pub amount: RecipePreparationStepIngredientAmount,
}


#[derive(GraphQLInputObject)]
#[graphql(description = "A new recipe preparation step ingredient, used where recipe_preparation_step_id is known")]
pub struct NewRecipePreparationStepIngredientNestedGqlInput {
    pub ingredient_id: IngredientId,
    pub unit_id: UnitId,
    pub amount: RecipePreparationStepIngredientAmount,
}

impl From<RecipePreparationStepIngredient> for RecipePreparationStepIngredientGql {
    fn from(rpsi: RecipePreparationStepIngredient) -> Self {
        RecipePreparationStepIngredientGql {
            id: rpsi.id,
            ingredient_id: rpsi.ingredient_id,
            recipe_preparation_step_id: rpsi.recipe_preparation_step_id,
            recipe_preparation_step: None,

            unit_id: rpsi.unit_id,
            unit: None,

            amount: rpsi.amount,
        }
    }
}
impl RecipePreparationStepIngredientGql {
    pub fn from_model_vec(input: Vec<RecipePreparationStepIngredient>) -> Vec<Self> {
        input
            .into_iter()
            .map(|rpsi| RecipePreparationStepIngredientGql::from(rpsi))
            .collect::<Vec<Self>>()
    }
}

graphql_object!(RecipePreparationStepIngredientGql: GraphQLContext as "RecipePreparationStepIngredient" |&self| {
    field id() -> &RecipePreparationStepIngredientId {
        &self.id
    }

    field amount() -> &RecipePreparationStepIngredientAmount {
        &self.amount
    }

    field recipe_preparation_step(&executor) -> FieldResult<RecipePreparationStepGql> {
        recipe_preparation_step_resolver(&self.recipe_preparation_step_id, executor)
    }

    field unit(&executor) -> FieldResult<UnitGql> {
        unit_resolver(&self.unit_id, executor)
    }

});