use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::recipe_preparation_step_ingredient;
use crate::modules::ingredient::model::IngredientId;
use crate::modules::recipe_preparation_step::model::{RecipePreparationStep, RecipePreparationStepId};
use crate::modules::recipe_preparation_step_ingredient::gql::entity::NewRecipePreparationStepIngredientGqlInput;
use crate::modules::unit::model::UnitId;

pub type RecipePreparationStepIngredientId = i32;
pub type RecipePreparationStepIngredientAmount = i32;

#[table_name = "recipe_preparation_step_ingredient"]
#[derive(Serialize, Identifiable, Associations, Queryable, Debug, Clone)]
#[belongs_to(RecipePreparationStep)]
pub struct RecipePreparationStepIngredient {
    pub id: RecipePreparationStepIngredientId,
    pub recipe_preparation_step_id: RecipePreparationStepId,
    pub ingredient_id: IngredientId,
    pub unit_id: UnitId,
    pub amount: RecipePreparationStepIngredientAmount,
}

#[table_name = "recipe_preparation_step_ingredient"]
#[derive(Serialize, Deserialize, Insertable)]
pub struct NewRecipePreparationStepIngredient {
    pub recipe_preparation_step_id: RecipePreparationStepId,
    pub ingredient_id: IngredientId,
    pub unit_id: UnitId,
    pub amount: RecipePreparationStepIngredientAmount,
}

impl From<NewRecipePreparationStepIngredientGqlInput> for NewRecipePreparationStepIngredient {
    fn from(input: NewRecipePreparationStepIngredientGqlInput) -> Self {
        NewRecipePreparationStepIngredient {
            ingredient_id: input.ingredient_id,
            recipe_preparation_step_id: input.recipe_preparation_step_id,
            unit_id: input.unit_id,
            amount: input.amount,
        }
    }
}

impl RecipePreparationStepIngredient {
    pub fn get_by_id(id: &RecipePreparationStepIngredientId, conn: &DbConnection) -> Result<RecipePreparationStepIngredient, Error> {
        recipe_preparation_step_ingredient::table
            .find(id)
            .first(conn)
    }

    pub fn find_by_recipe_preparation_step_id(id: &RecipePreparationStepId, conn: &DbConnection) -> Result<Vec<RecipePreparationStepIngredient>, Error> {
        recipe_preparation_step_ingredient::table
            .filter(recipe_preparation_step_ingredient::recipe_preparation_step_id.eq(id))
            .load(conn)
    }

    pub fn create(item: NewRecipePreparationStepIngredient, conn: &DbConnection) -> Result<RecipePreparationStepIngredient, Error> {
        diesel::insert_into(recipe_preparation_step_ingredient::table)
            .values(&item)
            .get_result(conn)
    }
}