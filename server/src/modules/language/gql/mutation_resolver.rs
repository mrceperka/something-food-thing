use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::language::gql::entity::LanguageGql;
use crate::modules::language::gql::entity::NewLanguageGqlInput;
use crate::modules::language::model::Language;
use crate::modules::language::model::NewLanguage;

pub fn create_resolver(input: NewLanguageGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<LanguageGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let lang = Language::create(NewLanguage::from(input), &conn)?;

    Ok(LanguageGql::from(lang))
}
