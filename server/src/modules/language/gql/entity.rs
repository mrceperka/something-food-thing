use crate::graphql::context::GraphQLContext;
use crate::modules::language::model::Language;
use crate::modules::language::model::LanguageCode;
use crate::modules::language::model::LanguageId;

#[graphql(description = "A language, used for translations")]
pub struct LanguageGql {
    pub id: LanguageId,
    pub code: LanguageCode,
}

#[derive(GraphQLInputObject)]
#[graphql(description = "A new language")]
pub struct NewLanguageGqlInput {
    pub unique_code: LanguageCode,
}

impl From<Language> for LanguageGql {
    fn from(i: Language) -> Self {
        LanguageGql {
            id: i.id,
            code: i.code,
        }
    }
}

graphql_object!(LanguageGql: GraphQLContext  as "Language" |&self| {
    field id() -> &LanguageId {
        &self.id
    }

    field code() -> &LanguageCode {
        &self.code
    }
});