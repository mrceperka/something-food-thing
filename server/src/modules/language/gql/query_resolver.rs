use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::language::gql::entity::LanguageGql;
use crate::modules::language::model::LanguageId;
use crate::modules::language::model::Language;


pub fn get_by_id_resolver(id: &LanguageId, executor: &Executor<GraphQLContext>) -> FieldResult<LanguageGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let lang = Language::get_by_id(id, &conn)?;

    Ok(LanguageGql::from(lang))
}
