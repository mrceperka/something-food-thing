use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::language;
use crate::modules::language::gql::entity::NewLanguageGqlInput;

pub type LanguageId = i32;
pub type LanguageCode = String;

#[table_name = "language"]
#[derive(Serialize, Queryable, Debug, Clone)]
pub struct Language {
    pub id: LanguageId,
    pub code: LanguageCode,
}


#[table_name = "language"]
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
pub struct NewLanguage {
    pub code: LanguageCode,
}

impl From<NewLanguageGqlInput> for NewLanguage {
    fn from(input: NewLanguageGqlInput) -> Self {
        NewLanguage {
            code: input.unique_code
        }
    }
}

impl Language {
    pub fn get_by_id(id: &LanguageId, conn: &DbConnection) -> Result<Language, Error> {
        language::table
            .find(id)
            .first(conn)
    }
    pub fn get_by_code(code: &LanguageCode, conn: &DbConnection) -> Result<Language, Error> {
        language::table
            .filter(language::code.eq(code))
            .first(conn)
    }
    pub fn find_all(conn: &DbConnection) -> Vec<Language> {
        language::table.order(language::id).load::<Language>(conn).unwrap()
    }
    pub fn get_default(conn: &DbConnection) -> Result<Language, Error> {
       Self::get_by_code(&String::from("en"), conn)
    }

    pub fn create(input: NewLanguage, conn: &DbConnection) -> Result<Language, Error> {
        diesel::insert_into(language::table)
            .values(&input)
            .get_result(conn)
    }
}

// code completion does not work...
// macro_rules! model {
//     ($model:ident, $schema:ident, $id_type:ty) => {
//          impl $model {
//             pub fn get_by_id(id: &$id_type, conn: &DbConnection) -> Result<$model, Error> {
//                 $schema::table
//                     .find(id)
//                     .first(conn)
//             }
//         }
//     };
// }
//
//
// model!(Language, language, LanguageId);