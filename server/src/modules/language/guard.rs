use rocket::outcome::Outcome;
use rocket::request::{self, FromRequest, Request};
use rocket::http::Status;
use crate::db::pool::DbConnectionRequestGuard as DbGuard;
use crate::modules::language::model::Language;

#[derive(Debug)]
pub struct LanguageGuard {
    pub language: Language
}

impl<'a, 'r> FromRequest<'a, 'r> for LanguageGuard {
    type Error = String;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let conn = request
            .guard::<DbGuard>()
            .expect("Couldn't retrieve DbGuard, please register it for given route.");

        let languages = Language::find_all(&conn);
        let default_language = match Language::get_default(&conn) {
            Ok(l) => l,
            _ => return Outcome::Failure((Status::InternalServerError, "Missing default language".to_string()))
        };

        let request_locale = request.headers()
            .get_one("Accept-Language")
            .unwrap_or(&default_language.code)
            .split(",")
            .filter_map(|lang| lang
                .split(|c| c == '-' || c == ';')
                .nth(0)
            )
            .nth(0)
            // Get the first requested locale we support
            //.find(|lang| locales.iter().any(|l| l.code == lang.to_string()))
            .unwrap_or(&default_language.code);

        let language = languages
            .into_iter()
            .find(|l| l.code == request_locale)
            .unwrap_or(default_language);

        Outcome::Success(LanguageGuard { language })
    }
}