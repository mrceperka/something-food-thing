use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient::gql::entity::IngredientFilterGqlInput;
use crate::modules::ingredient::gql::entity::IngredientGql;
use crate::modules::ingredient::model::{Ingredient, IngredientId};
use crate::modules::ingredient_translation::model::IngredientTranslation;
use crate::modules::ingredient_translation::model::IngredientTranslationName;
use crate::modules::language::model::LanguageId;

pub fn get_by_id_resolver(id: &IngredientId, executor: &Executor<GraphQLContext>) -> FieldResult<IngredientGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let ing = Ingredient::get_by_id(id, &conn)?;

    Ok(IngredientGql::from(ing))
}

pub fn filter_ingredients_resolver(input: &IngredientFilterGqlInput, executor: &Executor<GraphQLContext>) -> Vec<IngredientGql> {
    let ctx = executor.context();
    let lang = &ctx.language;
    let conn = match ctx.db_pool.get() {
        Ok(c) => c,
        _ => return vec![]
    };

    let filter = match (&input.name, &input.language_id) {
        (Some(_), None) => {
            IngredientFilterGqlInput {
                language_id: Some(lang.id),
                ..input.clone()
            }
        }
        _ => input.clone()
    };

    Ingredient::filter(&filter, &conn)
        .unwrap_or(vec![])
        .into_iter()
        .map(|i| {
            let mut gql = IngredientGql::from(i);
            gql.language_id = filter.language_id.clone();
            return gql;
        })
        .collect::<Vec<IngredientGql>>()
}

pub fn name_resolver(id: &IngredientId, language_id: &Option<LanguageId>, executor: &Executor<GraphQLContext>) -> FieldResult<Option<IngredientTranslationName>> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let lid = match language_id {
        Some(v) => v,
        _ => &ctx.language.id
    };
    match IngredientTranslation::get_by_ingredient_id_and_language_id(id, lid, &conn) {
        Ok(tr) => Ok(Some(tr.name)),
        _ => Ok(None)
    }
}
