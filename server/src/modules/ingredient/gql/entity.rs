use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient::gql::query_resolver::name_resolver;
use crate::modules::ingredient::model::Ingredient;
use crate::modules::ingredient::model::IngredientId;
use crate::modules::ingredient::model::IngredientName;
use crate::modules::ingredient_translation::gql::entity::NewIngredientTranslationNestedGqlInput;
use crate::modules::ingredient_translation::model::IngredientTranslationName;
use crate::modules::language::model::LanguageId;

#[graphql(description = "An ingredient, used in recipe preparations steps")]
pub struct IngredientGql {
    pub id: IngredientId,
    pub unique_name: IngredientName,
    pub name: Option<IngredientTranslationName>,

    // for translations
    pub language_id: Option<LanguageId>,
}

#[derive(GraphQLInputObject, Clone)]
#[graphql(description = "A new ingredient")]
pub struct NewIngredientGqlInput {
    pub unique_name: IngredientName,
    pub translations: Vec<NewIngredientTranslationNestedGqlInput>,
}

#[derive(GraphQLInputObject, Clone)]
#[graphql(description = "Ingredient filter")]
pub struct IngredientFilterGqlInput {
   // nothing here yet
   pub name: Option<IngredientTranslationName>,
   pub language_id: Option<LanguageId>
}

impl From<Ingredient> for IngredientGql {
    fn from(i: Ingredient) -> Self {
        IngredientGql {
            id: i.id,
            unique_name: i.name,
            name: None,
            language_id: None
        }
    }
}

graphql_object!(IngredientGql: GraphQLContext  as "Ingredient" |&self| {
    field id() -> &IngredientId {
        &self.id
    }

    field unique_name() -> &IngredientName {
        &self.unique_name
    }

     field name(&executor) -> FieldResult<Option<IngredientTranslationName>> {
        name_resolver(&self.id, &self.language_id, executor)
    }
});