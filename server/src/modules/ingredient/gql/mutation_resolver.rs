use juniper::Executor;
use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient::gql::entity::{IngredientGql, NewIngredientGqlInput};
use crate::modules::ingredient::model::{Ingredient, NewIngredient};
use crate::modules::ingredient_translation::model::IngredientTranslation;
use crate::modules::ingredient_translation::model::NewIngredientTranslation;

pub fn create_resolver(input: NewIngredientGqlInput, executor: &Executor<GraphQLContext>) -> FieldResult<IngredientGql> {
    let ctx = executor.context();
    let conn = ctx.db_pool.get()?;
    let ing = Ingredient::create(NewIngredient::from(input.clone()), &conn)?;
    for tr in input.translations {
        IngredientTranslation::create(NewIngredientTranslation {
            ingredient_id: ing.id,
            language_id: tr.language_id,
            name: tr.name,
        }, &conn)?;
    }

    Ok(IngredientGql::from(ing))
}
