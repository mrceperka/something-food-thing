use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::ingredient;
use crate::db::schema::ingredient_translation;
use crate::modules::ingredient::gql::entity::IngredientFilterGqlInput;
use crate::modules::ingredient::gql::entity::NewIngredientGqlInput;

pub type IngredientName = String;
pub type IngredientId = i32;

#[table_name = "ingredient"]
#[derive(Serialize, Queryable, Debug, Clone)]
pub struct Ingredient {
    pub id: IngredientId,
    pub name: IngredientName,
}

#[table_name = "ingredient"]
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
pub struct NewIngredient {
    pub name: IngredientName,
}

impl From<NewIngredientGqlInput> for NewIngredient {
    fn from(input: NewIngredientGqlInput) -> Self {
        NewIngredient {
            name: input.unique_name
        }
    }
}

impl Ingredient {
    pub fn get_by_id(id: &IngredientId, conn: &DbConnection) -> Result<Ingredient, Error> {
        ingredient::table
            .find(id)
            .first(conn)
    }

    pub fn filter(filter: &IngredientFilterGqlInput, conn: &DbConnection) -> Result<Vec<Ingredient>, Error> {
        let query = ingredient::table;

        if let (Some(n), Some(l)) = (&filter.name, &filter.language_id) {
            return ingredient::table
                .select(ingredient::all_columns)
                .inner_join(ingredient_translation::table.on(
                    ingredient_translation::ingredient_id.eq(ingredient::id)
                        .and(
                            ingredient_translation::language_id.eq(l)
                                .and(ingredient_translation::name.like(format!("%{}%", n)))
                        )))
                .load(conn);
        };

        query.load(conn)
    }


    pub fn create(ing: NewIngredient, conn: &DbConnection) -> Result<Ingredient, Error> {
        diesel::insert_into(ingredient::table)
            .values(&ing)
            .get_result(conn)
    }
}