use diesel::prelude::*;
use diesel::result::Error;

use crate::db::pool::DbConnection;
use crate::db::schema::recipe_preparation_step_translation;
use crate::modules::language::model::LanguageId;
use crate::modules::recipe_preparation_step::model::{RecipePreparationStep, RecipePreparationStepId};

pub type RecipePreparationStepTranslationDescription = String;
pub type RecipePreparationStepTranslationId = i32;

#[table_name = "recipe_preparation_step_translation"]
#[derive(Serialize, Identifiable, Associations, Queryable, Debug, Clone)]
#[belongs_to(RecipePreparationStep)]
pub struct RecipePreparationStepTranslation {
    pub id: RecipePreparationStepTranslationId,
    pub language_id: LanguageId,
    pub recipe_preparation_step_id: RecipePreparationStepId,
    pub description: RecipePreparationStepTranslationDescription,
}

#[table_name = "recipe_preparation_step_translation"]
#[derive(Serialize, Deserialize, Insertable)]
pub struct NewRecipePreparationStepTranslation {
    pub language_id: LanguageId,
    pub recipe_preparation_step_id: RecipePreparationStepId,
    pub description: RecipePreparationStepTranslationDescription,
}


impl RecipePreparationStepTranslation {
    pub fn get_by_recipe_preparation_step_id_and_language_id(id: &RecipePreparationStepTranslationId, lang_id: &LanguageId, conn: &DbConnection) -> Result<RecipePreparationStepTranslation, Error> {
        recipe_preparation_step_translation::table
            .filter(recipe_preparation_step_translation::recipe_preparation_step_id.eq(id))
            .filter(recipe_preparation_step_translation::language_id.eq(lang_id))
            .first(conn)
    }

    pub fn create(item: NewRecipePreparationStepTranslation, conn: &DbConnection) -> Result<RecipePreparationStepTranslation, Error> {
        diesel::insert_into(recipe_preparation_step_translation::table)
            .values(&item)
            .get_result(conn)
    }
}