#![feature(
plugin,
const_fn,
decl_macro,
custom_attribute,
proc_macro_hygiene,
uniform_paths,
never_type
)]
#![allow(proc_macro_derive_resolution_fallback, unused_attributes)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate juniper;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

use dotenv::dotenv;
use rocket::{catch, catchers};
use rocket_contrib::json;
use rocket_contrib::json::JsonValue;

use router::graphiql as GraphiQLRouter;
use router::graphql as GraphQLRouter;

use crate::context::Context;
use crate::graphql::mutation::Mutation;
use crate::graphql::query::Query;
use crate::graphql::schema::Schema;
use crate::modules::auth::router as AuthRouter;

mod modules;
mod db;
mod context;
mod router;
mod graphql;
mod config;

#[catch(404)]
fn not_found() -> JsonValue {
    json!({
        "status": "error",
        "reason": "Resource was not found."
    })
}

fn rocket() -> rocket::Rocket {
    dotenv().ok();

    let database_url = &config::CONFIG.database_url;

    let pool = db::pool::init(database_url);
    let ctx = Context::new(pool);
    let schema = Schema::new(
        Query,
        Mutation,
    );
    rocket::ignite()
        .manage(ctx)
        .manage(schema)
        .mount("/graphiql", GraphiQLRouter::routes())
        .mount("/graphql", GraphQLRouter::routes())
        .mount("/auth", AuthRouter::routes())
        .register(catchers![not_found])
}

fn main() {
    rocket().launch();
}
