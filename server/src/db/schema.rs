table! {
    ingredient (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    ingredient_translation (id) {
        id -> Int4,
        language_id -> Int4,
        ingredient_id -> Int4,
        name -> Varchar,
    }
}

table! {
    language (id) {
        id -> Int4,
        code -> Bpchar,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::modules::recipe::enums::difficulty::RecipeDifficultyType;
    recipe (id) {
        id -> Int4,
        difficulty -> RecipeDifficultyType,
    }
}

table! {
    recipe_preparation_step (id) {
        id -> Int4,
        position -> Int4,
        recipe_id -> Int4,
    }
}

table! {
    recipe_preparation_step_ingredient (id) {
        id -> Int4,
        recipe_preparation_step_id -> Int4,
        ingredient_id -> Int4,
        unit_id -> Int4,
        amount -> Int4,
    }
}

table! {
    recipe_preparation_step_translation (id) {
        id -> Int4,
        language_id -> Int4,
        recipe_preparation_step_id -> Int4,
        description -> Text,
    }
}

table! {
    recipe_translation (id) {
        id -> Int4,
        language_id -> Int4,
        recipe_id -> Int4,
        name -> Varchar,
    }
}

table! {
    unit (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    unit_translation (id) {
        id -> Int4,
        language_id -> Int4,
        unit_id -> Int4,
        name -> Varchar,
    }
}

table! {
    user (id) {
        id -> Int4,
        created_at -> Timestamptz,
    }
}

table! {
    user_credential (id) {
        id -> Int4,
        user_id -> Int4,
        username -> Varchar,
        password -> Varchar,
    }
}

table! {
    recipe_ingredient_view (recipe_id) {
        recipe_preparation_step_ingredient_id -> Int4,
        ingredient_id -> Int4,
        unit_id -> Int4,
        amount -> Int4,
        recipe_id -> Int4,
    }
}

joinable!(ingredient_translation -> ingredient (ingredient_id));
joinable!(ingredient_translation -> language (language_id));
joinable!(recipe_preparation_step -> recipe (recipe_id));
joinable!(recipe_preparation_step_ingredient -> ingredient (ingredient_id));
joinable!(recipe_preparation_step_ingredient -> recipe_preparation_step (recipe_preparation_step_id));
joinable!(recipe_preparation_step_ingredient -> unit (unit_id));
joinable!(recipe_preparation_step_translation -> language (language_id));
joinable!(recipe_preparation_step_translation -> recipe_preparation_step (recipe_preparation_step_id));
joinable!(recipe_translation -> language (language_id));
joinable!(recipe_translation -> recipe (recipe_id));
joinable!(unit_translation -> language (language_id));
joinable!(unit_translation -> unit (unit_id));
joinable!(user_credential -> user (user_id));

allow_tables_to_appear_in_same_query!(
    ingredient,
    ingredient_translation,
    language,
    recipe,
    recipe_preparation_step,
    recipe_preparation_step_ingredient,
    recipe_preparation_step_translation,
    recipe_translation,
    unit,
    unit_translation,
    user,
    user_credential,
    recipe_ingredient_view
);
