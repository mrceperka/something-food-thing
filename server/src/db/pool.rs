use diesel::pg::PgConnection;
use r2d2_diesel::ConnectionManager;

type ManagedPgConn = ConnectionManager<PgConnection>;
pub type DbPool = r2d2::Pool<ManagedPgConn>;

use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request, State};
use std::ops::Deref;
use diesel::pg::Pg;
use crate::context::Context;

/// Db Connection request guard type: wrapper around r2d2 pooled connection
pub struct DbConnectionRequestGuard(pub r2d2::PooledConnection<ManagedPgConn>);
pub type DbConnection = PgConnection;
pub type DbBackend = Pg;

/// Attempts to retrieve a single connection from the managed database pool. If
/// no pool is currently managed, fails with an `InternalServerError` status. If
/// no connections are available, fails with a `ServiceUnavailable` status.
impl<'a, 'r> FromRequest<'a, 'r> for DbConnectionRequestGuard {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<DbConnectionRequestGuard, ()> {
        let context = request.guard::<State<Context>>()?;
        match context.db_pool.get() {
            Ok(conn) => Outcome::Success(DbConnectionRequestGuard(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ())),
        }
    }
}

// For the convenience of using an &DbConn as an &PgConnection.
impl Deref for DbConnectionRequestGuard {
    type Target = PgConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub fn init(database_url: &str) -> DbPool {
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    r2d2::Pool::new(manager).expect("Failed to create pool.")
}
