use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::ingredient::gql::entity::{IngredientFilterGqlInput, IngredientGql};
use crate::modules::ingredient::gql::query_resolver::{filter_ingredients_resolver as filter_ingredients, get_by_id_resolver as get_ingredient};
use crate::modules::ingredient::model::IngredientId;
use crate::modules::ingredient_translation::gql::entity::IngredientTranslationGql;
use crate::modules::ingredient_translation::gql::query_resolver::get_by_id_resolver as get_ingredient_translation;
use crate::modules::ingredient_translation::model::IngredientTranslationId;
use crate::modules::language::gql::entity::LanguageGql;
use crate::modules::language::gql::query_resolver::get_by_id_resolver as get_language;
use crate::modules::language::model::LanguageId;
use crate::modules::recipe::gql::entity::{RecipeFilterGqlInput, RecipeGql};
use crate::modules::recipe::gql::query_resolver::filter_recipes_resolver as filter_recipes;
use crate::modules::recipe::gql::query_resolver::get_by_id_resolver as get_recipe;
use crate::modules::recipe::model::RecipeId;
use crate::modules::recipe_preparation_step::gql::entity::RecipePreparationStepGql;
use crate::modules::recipe_preparation_step::gql::query_resolver::get_by_id_resolver as get_rps;
use crate::modules::recipe_preparation_step::model::RecipePreparationStepId;
use crate::modules::recipe_preparation_step_ingredient::gql::entity::RecipePreparationStepIngredientGql;
use crate::modules::recipe_preparation_step_ingredient::gql::query_resolver::get_by_id_resolver as get_rpsi;
use crate::modules::recipe_preparation_step_ingredient::model::RecipePreparationStepIngredientId;
use crate::modules::unit::gql::entity::UnitGql;
use crate::modules::unit::gql::query_resolver::get_by_id_resolver as get_unit;
use crate::modules::unit::model::UnitId;
use crate::modules::unit_translation::gql::entity::UnitTranslationGql;
use crate::modules::unit_translation::gql::query_resolver::get_by_id_resolver as get_unit_translation;
use crate::modules::unit_translation::model::UnitTranslationId;
use crate::modules::user::gql::entity::UserGql;
use crate::modules::user::gql::query_resolver::get_by_id_resolver as get_user;
use crate::modules::user::model::UserId;
use crate::modules::user_credential::gql::entity::UserCredentialGql;
use crate::modules::user_credential::gql::query_resolver::get_by_id_resolver as get_user_credential;
use crate::modules::user_credential::model::UserCredentialId;

pub struct Query;

graphql_object!(Query: GraphQLContext as "Query" |&self| {
    field recipe(&executor, id: RecipeId) -> FieldResult<RecipeGql> {
        get_recipe(&id, executor)
    }

    field recipePreparationStep(&executor, id: RecipePreparationStepId) -> FieldResult<RecipePreparationStepGql> {
        get_rps(&id, executor)
    }

    field filterRecipes(&executor, filter: RecipeFilterGqlInput) -> Vec<RecipeGql> {
        filter_recipes(&filter, executor)
    }

    field user(&executor, id: UserId) -> FieldResult<UserGql> {
        get_user(&id, executor)
    }

    field filterIngredients(&executor, filter: IngredientFilterGqlInput) -> Vec<IngredientGql> {
        filter_ingredients(&filter, executor)
    }

    field ingredient(&executor, id: IngredientId) -> FieldResult<IngredientGql> {
        get_ingredient(&id, executor)
    }

    field ingredientTranslation(&executor, id: IngredientTranslationId) -> FieldResult<IngredientTranslationGql> {
        get_ingredient_translation(&id, executor)
    }

    field recipePreparationStepIngredient(&executor, id: RecipePreparationStepIngredientId) -> FieldResult<RecipePreparationStepIngredientGql> {
        get_rpsi(&id, executor)
    }

    field unit(&executor, id: UnitId) -> FieldResult<UnitGql> {
        get_unit(&id, executor)
    }

    field unitTranslation(&executor, id: UnitTranslationId) -> FieldResult<UnitTranslationGql> {
        get_unit_translation(&id, executor)
    }

    field language(&executor, id: LanguageId) -> FieldResult<LanguageGql> {
        get_language(&id, executor)
    }

    field userCredential(&executor, id: UserCredentialId) -> FieldResult<UserCredentialGql> {
        get_user_credential(&id, executor)
    }

});
