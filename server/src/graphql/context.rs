use crate::modules::language::model::Language;
use crate::db::pool::DbPool;
use juniper::Context;

pub struct GraphQLContext {
    pub language: Language,
    pub db_pool: DbPool,
}

impl Context for GraphQLContext {}
impl GraphQLContext {
    pub fn new(language: Language, db_pool: DbPool) -> Self {
        GraphQLContext {
            language,
            db_pool,

        }
    }
}