use juniper::FieldResult;

use crate::graphql::context::GraphQLContext;
use crate::modules::auth::gql::entity::AuthByUserCredentialsGql;
use crate::modules::auth::gql::mutation_resolver::login;
use crate::modules::ingredient::gql::entity::{IngredientGql, NewIngredientGqlInput};
use crate::modules::ingredient::gql::mutation_resolver::create_resolver as create_ingredient;
use crate::modules::ingredient_translation::gql::entity::{IngredientTranslationGql, NewIngredientTranslationGqlInput};
use crate::modules::ingredient_translation::gql::mutation_resolver::create_resolver as create_ingredient_translation;
use crate::modules::language::gql::entity::{LanguageGql, NewLanguageGqlInput};
use crate::modules::language::gql::mutation_resolver::create_resolver as create_lang;
use crate::modules::recipe::gql::entity::{NewRecipeGqlInput, RecipeGql};
use crate::modules::recipe::gql::mutation_resolver::create_resolver as create_recipe;
use crate::modules::recipe_preparation_step::gql::entity::{NewRecipePreparationStepGqlInput, RecipePreparationStepGql};
use crate::modules::recipe_preparation_step::gql::mutation_resolver::create_resolver as create_rps;
use crate::modules::unit::gql::entity::{NewUnitGqlInput, UnitGql};
use crate::modules::unit::gql::mutation_resolver::create_resolver as create_unit;
use crate::modules::unit_translation::gql::entity::{NewUnitTranslationGqlInput, UnitTranslationGql};
use crate::modules::unit_translation::gql::mutation_resolver::create_resolver as create_unit_translation;
use crate::modules::user::gql::entity::{NewUserGqlInput, UserGql};
use crate::modules::user::gql::mutation_resolver::create_resolver as create_user;

pub struct Mutation;
graphql_object!(Mutation: GraphQLContext as "Mutation" |&self| {
    field createRecipe(&executor, input: NewRecipeGqlInput) -> FieldResult<RecipeGql> {
        create_recipe(input, executor)
    }

    field login(&executor, username: String, password: String) -> FieldResult<AuthByUserCredentialsGql> {
        login(username, password, executor)
    }

    field createIngredient(&executor, input: NewIngredientGqlInput) -> FieldResult<IngredientGql> {
        create_ingredient(input, executor)
    }

    field createIngredientTranslation(&executor, input: NewIngredientTranslationGqlInput) -> FieldResult<IngredientTranslationGql> {
        create_ingredient_translation(input, executor)
    }

    field createUnit(&executor, input: NewUnitGqlInput) -> FieldResult<UnitGql> {
        create_unit(input, executor)
    }

    field createUnitTranslation(&executor, input: NewUnitTranslationGqlInput) -> FieldResult<UnitTranslationGql> {
        create_unit_translation(input, executor)
    }

    field createLanguage(&executor, input: NewLanguageGqlInput) -> FieldResult<LanguageGql> {
        create_lang(input, executor)
    }

    field createUser(&executor, input: NewUserGqlInput) -> FieldResult<UserGql> {
        create_user(input, executor)
    }

    field createRecipePreparationStep(&executor, input: NewRecipePreparationStepGqlInput) -> FieldResult<RecipePreparationStepGql> {
        create_rps(input, executor)
    }
});