use juniper::RootNode;

use crate::graphql::mutation::Mutation;
use crate::graphql::query::Query;

pub type Schema = RootNode<'static, Query, Mutation>;