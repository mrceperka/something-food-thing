use rocket::{post, Route, routes, State};

use crate::context::Context;
use crate::graphql::context::GraphQLContext;
use crate::graphql::schema::Schema;
use crate::modules::auth::guard::AuthToken;
use crate::modules::language::guard::LanguageGuard;

#[post("/", data = "<request>")]
fn post_graphql_handler(
    language_guard: LanguageGuard,
    ctx: State<Context>,
    request: juniper_rocket::GraphQLRequest,
    schema: State<Schema>,
    auth: AuthToken,
) -> juniper_rocket::GraphQLResponse {
    let gql_ctx = GraphQLContext::new(
        language_guard.language,
        ctx.db_pool.clone(),
    );
    println!("{:?}", auth);
    request.execute(&schema, &gql_ctx)
}

pub fn routes() -> Vec<Route> {
    routes![post_graphql_handler]
}