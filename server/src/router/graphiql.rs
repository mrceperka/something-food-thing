use rocket::{get,  Route, routes};
use rocket::response::content;

#[get("/")]
fn graphiql() -> content::Html<String> {
    juniper_rocket::graphiql_source("/graphql")
}

pub fn routes() -> Vec<Route> {
    routes![graphiql]
}