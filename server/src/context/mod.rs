use crate::db::pool::DbPool;

pub struct Context {
    pub db_pool: DbPool,
}

impl Context {
    pub fn new(database_pool: DbPool) -> Self {
        Context {
            db_pool: database_pool
        }
    }
}