CREATE TABLE "unit_translation" (
  "id" serial PRIMARY KEY,
  "language_id" integer NOT NULL REFERENCES "language"(id),
  "unit_id" integer NOT NULL REFERENCES unit(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  "name" VARCHAR NOT NULL,
  CONSTRAINT "unit_translation_language_id_unit_id_unique" UNIQUE ("language_id", "unit_id")
);