CREATE TABLE "user_credential" (
  "id" serial PRIMARY KEY,
  "user_id" integer NOT NULL REFERENCES "user"(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  "username" character varying NOT NULL,
  "password" character varying NOT NULL,
  CONSTRAINT user_credential_unique_username UNIQUE("username"),
  CONSTRAINT user_credential_unique_user_id UNIQUE("user_id")
);