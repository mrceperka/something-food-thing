CREATE VIEW "recipe_ingredient_view" AS SELECT
 "recipe_preparation_step_ingredient"."id" AS "recipe_preparation_step_ingredient_id",
 "recipe_preparation_step_ingredient"."ingredient_id" AS "ingredient_id",
 "recipe_preparation_step_ingredient"."unit_id" AS "unit_id",
 "recipe_preparation_step_ingredient"."amount" AS "amount",
"recipe"."id" AS "recipe_id"
 FROM "recipe" LEFT OUTER JOIN "recipe_preparation_step" ON "recipe_preparation_step"."recipe_id" = "recipe"."id"
 LEFT OUTER JOIN "recipe_preparation_step_ingredient" ON "recipe_preparation_step_ingredient"."recipe_preparation_step_id" = "recipe_preparation_step"."id"

