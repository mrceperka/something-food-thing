CREATE TABLE "recipe_translation" (
  "id" serial PRIMARY KEY,
  "language_id" integer NOT NULL REFERENCES "language"(id),
  "recipe_id" integer NOT NULL REFERENCES recipe(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  "name" VARCHAR NOT NULL,
  CONSTRAINT "recipe_translation_language_id_recipe_id_unique" UNIQUE ("language_id", "recipe_id")
);