CREATE TABLE "recipe_preparation_step_ingredient" (
  "id" serial PRIMARY KEY,
  "recipe_preparation_step_id" integer NOT NULL REFERENCES recipe_preparation_step(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  "ingredient_id" integer NOT NULL REFERENCES ingredient(id),
  "unit_id" integer NOT NULL REFERENCES unit(id),
  "amount" integer NOT NULL
);