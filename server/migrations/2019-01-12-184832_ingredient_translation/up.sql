CREATE TABLE "ingredient_translation" (
  "id" serial PRIMARY KEY,
  "language_id" integer NOT NULL REFERENCES "language"(id),
  "ingredient_id" integer NOT NULL REFERENCES ingredient(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  "name" VARCHAR NOT NULL,
  CONSTRAINT "ingredient_translation_language_id_ingredient_id_unique" UNIQUE ("language_id", "ingredient_id")
);