CREATE TABLE unit (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  CONSTRAINT unit_unique_name UNIQUE(name)
);
