CREATE TABLE "language" (
  id SERIAL PRIMARY KEY,
  code CHAR(2) NOT NULL,
  CONSTRAINT language_unique_code UNIQUE(code)
);

INSERT INTO "language" ("code")
VALUES ('cs'), ('en');