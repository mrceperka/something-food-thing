CREATE TABLE "recipe_preparation_step" (
  "id" serial PRIMARY KEY,
  "position" integer NOT NULL,
  "recipe_id" integer NOT NULL REFERENCES recipe(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT "recipe_preparation_step_position_recipe_id_unique" UNIQUE ("position", "recipe_id")
);