CREATE TABLE ingredient (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  CONSTRAINT ingredient_unique_name UNIQUE(name)
);
