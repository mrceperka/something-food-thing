CREATE TABLE "recipe_preparation_step_translation" (
  "id" serial PRIMARY KEY,
  "language_id" integer NOT NULL REFERENCES "language"(id),
  "recipe_preparation_step_id" integer NOT NULL REFERENCES recipe_preparation_step(id) ON DELETE CASCADE ON UPDATE NO ACTION,
  "description" text NOT NULL,
  CONSTRAINT "recipe_preparation_step_translation_language_id_recipe_preparation_step_id_unique" UNIQUE ("language_id", "recipe_preparation_step_id")
);