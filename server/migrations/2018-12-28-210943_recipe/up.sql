CREATE TYPE recipe_difficulty AS ENUM ('easy', 'medium', 'hard');
CREATE TABLE "recipe" (
  "id" serial PRIMARY KEY,
  "difficulty" recipe_difficulty NOT NULL
);